﻿package com.cupcake 
{
	import com.amazon.nativeextensions.android.AmazonItemData;
	import com.amazon.nativeextensions.android.AmazonPurchase;
	import com.amazon.nativeextensions.android.AmazonPurchaseReceipt;
	import com.amazon.nativeextensions.android.events.AmazonPurchaseEvent;
	import com.cupcake.App;
	import com.cupcake.Utils;
	import com.distriqt.extension.inappbilling.InAppBilling;
	import com.distriqt.extension.inappbilling.InAppBillingServiceTypes;
	import com.distriqt.extension.inappbilling.Product;
	import com.distriqt.extension.inappbilling.Purchase;
	import com.distriqt.extension.inappbilling.events.InAppBillingEvent;
	import com.greensock.TweenMax;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.XMLLoader;
	
	import org.osflash.signals.Signal;
	
	public class IAPManager 
	{
		
		public static const MARKET_AMAZON:String		= "amazon";
		public static const MARKET_GOOGLE:String		= "google";
		public static const MARKET_ITUNES:String		= "itunes";
		public static const MARKET_NONE:String			= "none";
		
		private static const DEFAULT_CATALOG:String		= "data/catalog.xml";
		private static const MANIFEST_FILE:String		= "manifest.xml";
		
		public static var bypassMode:Boolean			= false;
		public static var devMode:Boolean				= false;
		public static var catalogLoaded:Signal			= new Signal();
		public static var manifestUpdated:Signal		= new Signal();
		public static var actionCompleted:Signal		= new Signal( Boolean );
		
		public static var catalog:Vector.<IAProduct>	= null;
		
		private static var _enabled:Boolean				= false;
		private static var catalogFile:String			= DEFAULT_CATALOG;
		private static var devKey:String				= "";
		private static var encryptionKey:String			= "";
		private static var market:String				= MARKET_NONE;
		private static var supported:Boolean			= false;
		
		private static var ap:AmazonPurchase			= null;
		private static var ids:Array					= null;
		private static var service:InAppBilling			= null;
		
		public static function get enabled():Boolean	{ return _enabled; }
		
		public static function Init( appMarket:String, xmlPath:String = "" ):void
		{
			market			= ( appMarket == MARKET_GOOGLE || appMarket == MARKET_ITUNES || appMarket == MARKET_AMAZON ) ? appMarket : MARKET_NONE;
			catalogFile		= xmlPath != "" ? xmlPath : catalogFile;
			
			// Set up markets here so we can know immediately after init 
			// if the IAP service is available
			_enabled = ( market == MARKET_GOOGLE || market == MARKET_ITUNES || market == MARKET_AMAZON || devMode );
			
			supported = ( ( market == MARKET_GOOGLE || market == MARKET_ITUNES ) && InAppBilling.isSupported ) ||
					      ( market == MARKET_AMAZON && AmazonPurchase.isSupported() ) || devMode;

			if ( !bypassMode )
			{
				var loader:XMLLoader = new XMLLoader( catalogFile, { onComplete: CatalogLoaded } );
				loader.load();
			}
		}
		
		public static function ConsumeProduct( productName:String, byID:Boolean = false, qty:int = 1 ):void
		{
			var product:IAProduct = GetProduct( productName, byID );
			
			if ( product != null )
			{
				product.purchased = false;
				SaveManifest();
				
				if ( devMode ) { TweenMax.delayedCall( 1, SignalUpdate, null, true ); }
				else
				{
					if ( market == MARKET_ITUNES || market == MARKET_GOOGLE )
					{
						var purchase:com.distriqt.extension.inappbilling.Purchase = new com.distriqt.extension.inappbilling.Purchase( product.id, qty );
						service.consumePurchase( purchase );
						App.Log( "IAPManager: Consuming purchase of product '" + productName + "'" );
					}
					else if ( market == MARKET_AMAZON )
						{ App.Log( "IAPManager: AmazonPurchase cannot consume purchases." ); }
				}
			}
		}
		
		public static function GetProduct( productName:String, byID:Boolean = false ):IAProduct
		{
			var prod:IAProduct = null;
			for each ( var product:IAProduct in catalog )
			{
				if ( product.name == productName || ( byID && product.id == productName ) )
				{
					prod = product;
					break;
				}
			}
			if ( prod == null ) { App.Log( "IAPManager: Product '" + productName + "' not found." ); }
			return prod;
		}
		
		public static function Grant( productName:String, byID:Boolean = false ):Boolean
		{
			var prod:IAProduct	= GetProduct( productName, byID );
			var res:Boolean		= prod != null;
			if ( res ) 			{ prod.purchased = true; SaveManifest(); }
			return res;
		}
		
		public static function IsPurchased( productName:String, byID:Boolean = false ):Boolean
		{
			if ( bypassMode ) { return true; }
			var prod:IAProduct	= GetProduct( productName, byID );
			var res:Boolean		= prod != null ? prod.purchased : false;
			return res;
		}
		
		public static function Purchase( productName:String, byID:Boolean = false ):void
		{
			if ( !enabled || !supported ) { return; }
			
			var prod:IAProduct	= GetProduct( productName, byID );
			if ( prod == null ) { return; }
			
			if ( devMode )
			{
				Grant( productName, byID );
				TweenMax.delayedCall( 1, SignalUpdate, null, true );
				TweenMax.delayedCall( 1, SignalAction, null, true );
			}
			else if ( market == MARKET_GOOGLE || market == MARKET_ITUNES )
			{
				var purchase:com.distriqt.extension.inappbilling.Purchase = new com.distriqt.extension.inappbilling.Purchase( prod.id );
				var result:Boolean = service.makePurchase( purchase );
				App.Log( "IAPManager: InAppBilling " + ( result ? "starting" : "cannot start" ) + " purchase of '" + productName + "'" );
			}
			else if ( market == MARKET_AMAZON )
			{
				ap.purchaseItem( prod.id );
				App.Log( "IAPManager: AmazonPurchase starting purchase of '" + productName + "'" );
			}
			
			App.Log( "IAPManager: Purchase of product '" + productName + "' initiated." );
		}
		
		public static function Restore():void
		{
			if ( !enabled || !supported ) { return; }
			
			if ( devMode ) { TweenMax.delayedCall( 1, SignalUpdate, null, true ); TweenMax.delayedCall( 1, SignalAction, null, true ); }
			else if ( market == MARKET_GOOGLE || market == MARKET_ITUNES )
			{
				App.Log( "IAPManager: InAppBilling restoring purchases" );
				service.restorePurchases();
			}
			else if ( market == MARKET_AMAZON )
			{
				App.Log( "IAPManager: AmazonPurchase restoring purchases" );
				ap.restoreTransactions(); 
			}
		}
		
		public static function RevokeAll():void
		{
			// This function is for TESTING ONLY!
			// Do not make this call on a live app!
			for each ( var iap:IAProduct in catalog ) { ConsumeProduct( iap.name ); }
			App.Log( "IAPManager: All products revoked" );
		}

		// private static functions
		private static function LoadManifest():void
		{
			var manifestString:String	= Utils.loadStringFile( MANIFEST_FILE );
			if ( manifestString.length == 0 ) { SaveManifest(); return; }
			
			var manifestXML:XML			= new XML( manifestString );
			var productList:XMLList		= manifestXML.Product;
			
			for each ( var product:XML in productList )
			{
				if ( product.Purchased.toString() == "true" )
				{
					var prod:IAProduct = GetProduct( product.Name.toString() );
					if ( prod != null ) 
					{ 
						prod.purchased	= true;
						prod.price		= product.Price.toString() != "" ? product.Price.toString() : prod.price;
						prod.title		= product.Title.toString() != "" ? product.Title.toString() : prod.title;
					}
				}
			}
			
			App.Log( "IAPManager: Manifest loaded" );
		}
		
		private static function CatalogLoaded( e:LoaderEvent ):void
		{
			catalog 				= new Vector.<IAProduct>;
			var catalogXML:XML 		= ( e.target as XMLLoader ).content;
			var productList:XMLList	= catalogXML.Product;
			
			devKey 					= catalogXML.DevKey[0].toString();
			encryptionKey			= market == MARKET_GOOGLE ? catalogXML.EncryptionKey[0].toString() : "";
			ids						= new Array;
			
			// build local catalog
			for each ( var product:XML in productList )
			{
				var iap:IAProduct	= new IAProduct;
				iap.name			= product.Name.toString();
				iap.id				= product.ID.toString();
				if ( devMode )
				{
					iap.price 		= product.DevPrice.toString();
					iap.title 		= product.DevTitle.toString();
				}
				if ( market == MARKET_NONE ) { iap.purchased = true; }
				catalog.push( iap );
				ids.push( iap.id );
			}
			
			// load up local manifest
			LoadManifest();
			SignalLoaded();
			
			// Only attempt IAP service setup if we're enabled and supported
			if ( !enabled || !supported ) { return; }
			
			// setup IAP libraries
			if ( devMode ) { TweenMax.delayedCall( 1, SignalUpdate, null, true ); TweenMax.delayedCall( 1, SignalAction, null, true ); }			
			else if ( market == MARKET_AMAZON )
			{			
				AmazonPurchase.create();
				ap = AmazonPurchase.amazonPurchase;
				
				Utils.addHandler( ap, AmazonPurchaseEvent.ITEM_DATA_FAILED, 			AP_DataFailed );
				Utils.addHandler( ap, AmazonPurchaseEvent.ITEM_DATA_LOADED,				AP_DataLoaded );
				
				Utils.addHandler( ap, AmazonPurchaseEvent.PURCHASE_ALREADY_ENTITLED,	AP_AlreadyEntitled );
				Utils.addHandler( ap, AmazonPurchaseEvent.PURCHASE_FAILED,				AP_PurchaseFailed );
				Utils.addHandler( ap, AmazonPurchaseEvent.PURCHASE_SKU_INVALID,			AP_SKUInvalid );
				Utils.addHandler( ap, AmazonPurchaseEvent.PURCHASE_SUCCEEDED,			AP_PurchaseSucceeded );
				
				Utils.addHandler( ap, AmazonPurchaseEvent.PURCHASES_UPDATE_FAILED,		AP_UpdateFailed );
				Utils.addHandler( ap, AmazonPurchaseEvent.PURCHASES_UPDATED,			AP_PurchasesUpdated );
				
				AP_RetrieveData();
			}
			else if ( market == MARKET_GOOGLE || market == MARKET_ITUNES )
			{
				var serviceType:String = market == MARKET_ITUNES ? InAppBillingServiceTypes.APPLE_INAPP_PURCHASE :
																   InAppBillingServiceTypes.GOOGLE_PLAY_INAPP_BILLING;
				
				InAppBilling.init( devKey );
				service = InAppBilling.service;
				service.setServiceType( serviceType );
				
				Utils.addHandler( service, InAppBillingEvent.SETUP_SUCCESS, 			IAB_SetupSucceeded );
				Utils.addHandler( service, InAppBillingEvent.SETUP_FAILURE, 			IAB_SetupFailed );
				
				Utils.addHandler( service, InAppBillingEvent.PRODUCTS_LOADED,			IAB_ProductsLoaded );
				Utils.addHandler( service, InAppBillingEvent.PRODUCTS_FAILED,			IAB_ProductsFailed );
				Utils.addHandler( service, InAppBillingEvent.INVALID_PRODUCT,			IAB_InvalidProduct );
				
				Utils.addHandler( service, InAppBillingEvent.PURCHASE_CANCELLED, 		IAB_PurchaseCancelled );
				Utils.addHandler( service, InAppBillingEvent.PURCHASE_FAILED, 			IAB_PurchaseFailed );
				Utils.addHandler( service, InAppBillingEvent.PURCHASE_SUCCESS, 			IAB_PurchaseSucceeded );
				
				Utils.addHandler( service, InAppBillingEvent.RESTORE_PURCHASES_SUCCESS,	IAB_RestoreSucceeded );
				Utils.addHandler( service, InAppBillingEvent.RESTORE_PURCHASES_FAILED,	IAB_RestoreFailed );
				
				Utils.addHandler( service, InAppBillingEvent.CONSUME_SUCCESS,			IAB_ConsumeSucceeded );
				Utils.addHandler( service, InAppBillingEvent.CONSUME_FAILED,			IAB_ConsumeFailed );
				
				IAB_SetupService();
			}
		}		
		
		private static function SaveManifest():void
		{
			var xmlString:String = "<Manifest>\n";
			
			for each ( var iap:IAProduct in catalog )
			{
				xmlString += "\t<Product>\n\t\t<Name>" + iap.name + "</Name>\n";
				xmlString += "\t\t<Purchased>" + iap.purchased.toString() + "</Purchased>\n";
				xmlString += "\t\t<Price>" + iap.price.toString() + "</Price>\n";
				xmlString += "\t\t<Title>" + iap.title.toString() + "</Title>\n";
				xmlString += "\t</Product>\n";
			}
			
			xmlString += "</Manifest>\n";
			Utils.saveStringFile( xmlString, MANIFEST_FILE );
			
			App.Log( "IAPManager: Manifest saved" );
		}
		
		private static function SignalAction( success:Boolean = true ):void 
			{ actionCompleted.dispatch( success ); App.Log( "IAPManager: Action completed" ); }
		
		private static function SignalLoaded():void { catalogLoaded.dispatch(); App.Log( "IAPManager: Catalog loaded" ); }
		private static function SignalUpdate():void { manifestUpdated.dispatch(); App.Log( "IAPManager: Product updated" ); }

		// ****************************************
		// AmazonPurchase Handlers
		// ****************************************
		private static function AP_AlreadyEntitled( e:AmazonPurchaseEvent ):void
		{
			var product:IAProduct;
			
			for each( var receipt:AmazonPurchaseReceipt in e.receipts )
			{
				product = GetProduct( receipt.sku, true );
				if ( product != null ) 
				{ 
					product.purchased = true; 
					App.Log( "IAPManager: You already bought a '" + product.name  + "'" );
				}
			}
			
			SaveManifest();
			SignalUpdate();
			SignalAction( false );
		}
		
		private static function AP_PurchaseSucceeded( e:AmazonPurchaseEvent ):void
		{
			var product:IAProduct;
			
			for each( var receipt:AmazonPurchaseReceipt in e.receipts )
			{
				product = GetProduct( receipt.sku, true );
				if ( product != null ) 
				{ 
					product.purchased = true; 
					App.Log( "IAPManager: AmazonPurchase Purchase of item '" + product.name  + "' succeeded" );
				}
			}
			
			SaveManifest();
			SignalUpdate();
			SignalAction();
		}
		
		private static function AP_PurchaseFailed( event:AmazonPurchaseEvent ):void
			{ App.Log( "IAPManager: AmazonPurchase purchase failed" ); SignalAction( false ); }
			
		private static function AP_SKUInvalid( event:AmazonPurchaseEvent ):void
			{ App.Log( "IAPManager: AmazonPurchase SKU does not exist" ); SignalAction( false ); }
		
		private static function AP_PurchasesUpdated( e:AmazonPurchaseEvent ):void
		{
			App.Log( "IAPManager: AmazonPurchase updating purchases" );
			
			var product:IAProduct;
			
			for each( var receipt:AmazonPurchaseReceipt in e.receipts )
			{
				product = GetProduct( receipt.sku, true );
				if ( product != null ) 
				{ 
					product.purchased = true; 
					App.Log( "IAPManager: You previously bought a '" + product.name  + "'" );
				}
			}
			
			SaveManifest();
			SignalUpdate();
			SignalAction();
		}
		
		private static function AP_UpdateFailed( e:AmazonPurchaseEvent ):void
			{ App.Log( "IAPManager: AmazonPurchase failed to update products" ); }
			
		private static function AP_DataFailed( e:AmazonPurchaseEvent ):void
		{ 
			App.Log( "IAPManager: AmazonPurchase failed to retrieve product data" ); 
			TweenMax.delayedCall( 5, AP_RetrieveData );
		}
		
		private static function AP_RetrieveData():void
			{ ap.loadItemData( ids ); App.Log( "IAPManager: AmazonPurchase retrieving product data" ); }
		
		private static function AP_DataLoaded( e:AmazonPurchaseEvent ):void
		{
			var product:IAProduct;
			
			App.Log( "IAPManager: AmazonPurchase processing product info" );
			for each( var item:AmazonItemData in e.itemDatas )
			{
				App.Log( "IAPManager: sku = '" + item.sku + "', price = " + item.price + ", iconUrl = '" + item.smallIconUrl + "', title = '" + item.title + "', description = '" + item.description + "'");
				product = GetProduct( item.sku, true );
				if ( product != null )
				{
					product.price = item.price;
					product.title = item.title;
				}
			}
		}		
		
		// ****************************************
		// InAppBilling Handlers
		// ****************************************
		private static function IAB_SetupSucceeded( event:InAppBillingEvent ):void
		{
			App.Log( "IAPManager: InAppBilling setup success, retrieving products" );
			IAB_RetrieveProducts();
		}
		
		private static function IAB_SetupFailed( event:InAppBillingEvent ):void
		{ 
			App.Log( "IAPManager: InAppBilling setup failed" ); 
			TweenMax.delayedCall( 5, IAB_SetupService );
		}
			
		private static function IAB_SetupService():void 
			{ App.Log( "IAPManager: InAppBilling setting up service" ); service.setup( encryptionKey ); }
			
		private static function IAB_RetrieveProducts():void
			{ App.Log( "IAPManager: InAppBilling retrieving products" ); service.getProducts( ids ); }
			
		private static function IAB_ProductsLoaded( event:InAppBillingEvent ):void
		{
			var prod:IAProduct;
			
			App.Log( "IAPManager: InAppBilling products loaded" );
			for each ( var product:Product in event.data )
			{
				prod = GetProduct( product.id, true );
				if ( prod != null )
				{
					prod.title = product.title;
					prod.price = product.priceString;
				}
			}
			
			SaveManifest();
			SignalUpdate();
			
			if ( market == MARKET_GOOGLE ) { Restore(); }
		}
		
		private static function IAB_ProductsFailed( event:InAppBillingEvent ):void
			{ App.Log( "IAPManager: InAppBilling products retrieval failed" ); }
			
		private static function IAB_InvalidProduct( event:InAppBillingEvent ):void
			{ App.Log( "IAPManager: InAppBilling Product not valid" ); SignalAction( false ); }
			
		private static function IAB_PurchaseCancelled( event:InAppBillingEvent ):void
			{ App.Log( "IAPManager: InAppBilling Product purchase cancelled" ); SignalAction( false ); }
		
		private static function IAB_PurchaseFailed( event:InAppBillingEvent ):void
			{ App.Log( "IAPManager: InAppBilling Product purchase failed [" + event.errorCode + "] :: " + event.message ); SignalAction( false ); }
			
		private static function IAB_PurchaseSucceeded( event:InAppBillingEvent ):void
		{
			var product:IAProduct;
			var purchase:com.distriqt.extension.inappbilling.Purchase;
			
			for each ( purchase in event.data )
			{
				product = GetProduct( purchase.productId, true );
				if ( product != null ) 
				{ 
					product.purchased = true; 
					App.Log( "IAPManager: InAppBilling Purchase of product '" + product.name + "' complete." );
				}
			}
			
			SaveManifest();
			SignalUpdate();
			SignalAction();
		}
			
		private static function IAB_RestoreSucceeded( event:InAppBillingEvent ):void
			{ App.Log( "IAPManager: InAppBilling Restore purchases successful" ); SignalAction(); }
			
		private static function IAB_RestoreFailed( event:InAppBillingEvent ):void
			{ App.Log( "IAPManager: InAppBilling Restore purchases failed" ); SignalAction( false ); }
			
		private static function IAB_ConsumeSucceeded( event:InAppBillingEvent ):void
		{ 
			var product:IAProduct;
			var purchase:com.distriqt.extension.inappbilling.Purchase;
			
			for each ( purchase in event.data )
			{
				product = GetProduct( purchase.productId, true );
				if ( product != null ) { product.purchased = false; }
			}
			
			SaveManifest();
			App.Log( "IAPManager: InAppBilling Consumption of product '" + product.name + "' successful." );
			SignalUpdate();
			SignalAction();
		}
			
		private static function IAB_ConsumeFailed( event:InAppBillingEvent ):void
			{ App.Log( "IAPManager: InAppBilling Consume purchase failed" ); }
	}
}
