﻿package com.cupcake
{
	public class IAProduct 
	{
		public var id:String			= ""; // the market IAP id (ex. com.cupcakedigital.dev.iapdev.winterpack)
		public var name:String			= ""; // the short name for reference in-app (ex. "winter_pack")
		public var price:String			= ""; // the localized display value for the product price (ex. "$1.99", "€.99")
		public var purchased:Boolean	= false;
		public var title:String			= ""; // the localized display name for the product (ex. "Winter Pack")
	}
}
