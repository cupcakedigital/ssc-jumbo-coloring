﻿package com.cupcake
{
	import com.greensock.TweenMax;
	
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.getTimer;
	
	import scenes.Pages.PageManager;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.utils.AssetManager;
	
	public class BookPage extends Sprite
	{
		// Scene Objects
		public var sceneRoot:Sprite;
		public var bg:Image;
		
		public var Reader_BOX:Sprite;
		
		public var backButton:Image;
		
		public var CueVector:Vector.<int>;
		public var WordBoxes:Vector.<TextField> = new <TextField>[];
		public var NarrationSounds:Vector.<Sound> = new Vector.<Sound>();
		
		// Standard display
		public var stageWidth:int;
		public var stageHeight:int;
		
		public var wordsArray:Array;
		
		public var Text:TextField;
		
		public var pagenum:int;
		
		public var NarrationChannel:SoundChannel = new SoundChannel();
		
		
		public var BackIMG:Image;
		public var ForwardIMG:Image;
		public var MenuIMG:Image;
		public var Mute1IMG:Image;
		public var Mute2IMG:Image;
		
		public var BackButton:Sprite;
		public var ForwardButton:Sprite;
		public var MenuButton:Sprite;
		public var MuteButton:Sprite;

		
		public var words:Vector.<String>;
		
		public var PreviousTime:int = 0;
		public var CurrentTime:int = 0;
		public var ElapsedTime:int = 0;
		public var CueTimer:int = 0;
		
		public var assets:AssetManager;
		public var soundManager:SoundManager;
		public var appDir:File = File.applicationDirectory;
		
		public var paused:Boolean = false;
		private var NarrationPausePoint:int = 0;
		
		public var NarrationImage:Image;
		
		public var NarrationDone:Boolean = false;
		
		
		public function BookPage()
		{
		}
		
		public function AddReaderBox():void{
			Reader_BOX = new Sprite();
			
			sceneRoot.addChild(Reader_BOX);
			
			NarrationImage = new Image(assets.getTexture("nutjob_textbox_v2"));
			
			Reader_BOX.addChild(NarrationImage);
			Reader_BOX.y = Costanza.STAGE_HEIGHT - Reader_BOX.height;
			
			BackIMG = new Image(assets.getTexture("ArrowIMG"));
			ForwardIMG = new Image(assets.getTexture("ArrowIMG"));
			MenuIMG = new Image(assets.getTexture("Home"));
			
			Mute1IMG = new Image(assets.getTexture("_0000_mute_off"));
			Mute2IMG = new Image(assets.getTexture("_0001_mute_on"));
			
			BackButton = new Sprite();
			ForwardButton = new Sprite();
			MenuButton = new Sprite();
			MuteButton = new Sprite();
			
			ForwardButton.x = 153;
			ForwardButton.y = 62;
			
			BackButton.y = 0;
			BackButton.x = 1366-171-ForwardIMG.width;
			ForwardIMG.rotation = -1;
			
			BackButton.addChild(BackIMG);
			ForwardButton.addChild(ForwardIMG);
			MenuButton.addChild(MenuIMG);
			if(PageManager.muted)
			{
				MuteButton.addChild(Mute1IMG);
			}
			else
			{
				MuteButton.addChild(Mute2IMG);
			}
			
			MenuButton.x = 173;
			MenuButton.y = 630;
			
			BackButton.alpha = 0;
			ForwardButton.alpha = 0;
			MenuButton.alpha = 0;
			MuteButton.x = 1080, 
			MuteButton.y = 640;
			
			sceneRoot.addChild(BackButton);
			sceneRoot.addChild(ForwardButton);
			sceneRoot.addChild(MenuButton);
			sceneRoot.addChild(MuteButton);

			var cuesArray:Array = Root.NarrationXML.Page[pagenum].CuePoints.split(", ");
			CueVector = new <int>[];
			
			for (var j:int = 0; j < cuesArray.length; j++)
			{
				CueVector.push(int(cuesArray[j]));
			}
			
			
			for(var x:int = 0; x < words.length; x++)
			{
				Text = null;
				Text = new TextField(1,1,words[x], Root.NarrationXML.@FontClass, Root.NarrationXML.@FontSize);
				Text.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
				Text.addEventListener(TouchEvent.TOUCH, PressWord);
				WordBoxes.push(Text);
			}
		}
		
		public function PositionWords():void
		{
			//x:315 y:40 w:765 h:120
			var LineOne:Sprite = new Sprite();
			var LineTwo:Sprite = new Sprite();
			var LineThree:Sprite = new Sprite();
			var LineFour:Sprite = new Sprite();
			
			var Line1Done:Boolean = false;
			var Line2Done:Boolean = false;
			var Line3Done:Boolean = false;
			
			for(var x:int = 0; x < WordBoxes.length; x++)
			{
				if(((LineOne.width + WordBoxes[x].width) < 765) && !Line1Done)
				{
					WordBoxes[x].x = LineOne.width;
					LineOne.addChild(WordBoxes[x]);
				}
				else if(((LineTwo.width + WordBoxes[x].width) < 765) && !Line2Done)
				{
					Line1Done = true;
					WordBoxes[x].x = LineTwo.width;
					LineTwo.addChild(WordBoxes[x]);
				}
				else if(((LineThree.width + WordBoxes[x].width) < 765) && !Line3Done)
				{
					Line2Done = true;
					WordBoxes[x].x = LineThree.width;
					LineThree.addChild(WordBoxes[x]);
				}
				else if((LineFour.width + WordBoxes[x].width) < 765)
				{
					Line3Done = true;
					WordBoxes[x].x = LineFour.width;
					LineFour.addChild(WordBoxes[x]);
				}
			}
			
			if(LineFour.width > 0)
			{
				LineOne.x = (315 + (765 - LineOne.width)/2);
				LineOne.y = 40;
				LineTwo.x = (315 + (765 - LineTwo.width)/2);
				LineTwo.y = 70;
				LineThree.x = (315 + (765 - LineThree.width)/2);
				LineThree.y = 100;
				LineFour.x = (315 + (765 - LineFour.width)/2);
				LineFour.y = 130;
				
				Reader_BOX.addChild(LineOne);
				Reader_BOX.addChild(LineTwo);
				Reader_BOX.addChild(LineThree);
				Reader_BOX.addChild(LineFour);
			}
			else if(LineThree.width > 0)
			{
				LineOne.x = (315 + (765 - LineOne.width)/2);
				LineOne.y = 40;
				LineTwo.x = (315 + (765 - LineTwo.width)/2);
				LineTwo.y = 70;
				LineThree.x = (315 + (765 - LineThree.width)/2);
				LineThree.y = 100;
				
				Reader_BOX.addChild(LineOne);
				Reader_BOX.addChild(LineTwo);
				Reader_BOX.addChild(LineThree);
			}
			else if(LineTwo.width > 0)
			{
				LineOne.x = (315 + (765 - LineOne.width)/2);
				LineOne.y = 50;
				LineTwo.x = (315 + (765 - LineTwo.width)/2);
				LineTwo.y = 80;
				
				Reader_BOX.addChild(LineOne);
				Reader_BOX.addChild(LineTwo);
			}
			else
			{
				LineOne.x = (315 + (765 - LineOne.width)/2);
				LineOne.y = 60;
				
				Reader_BOX.addChild(LineOne);
			}
		}
		
		public function PressWord(e:TouchEvent):void
		{
			if(NarrationDone || PageManager.muted)
			{
				var touch:Touch = e.getTouch(stage);
				if(touch == null){return;}
				if(touch.phase == TouchPhase.BEGAN)
				{
				Text = e.currentTarget as TextField;
					for(var x:int = 0; x < words.length; x++)
					{
						if(Text.text == words[x])
						{
							var WordSound:Sound;
							//WordSound = assets.getSound("day");
							WordSound = assets.getSound(words[x].split(" ").join("").split(".").join("").split(",").join("").split("!").join("").split("-").join("").split("\"").join("").split(":").join("").toUpperCase());
							WordSound.play();
							x = words.length;
							Text.color = 0xFFFFFF;
							TweenMax.delayedCall(0.5, FadeText, [Text]);
						}
					}
				};
			}
		}
		
		public function FadeText(text:TextField):void
		{
			text.color = 0x000000;
		}
		
		public function cuePointTimer(e:Event):void
		{
			if(!PageManager.muted && !NarrationDone)
			{
				PreviousTime = CurrentTime;
				//var holdertime:int = CurrentTime;
				CurrentTime = flash.utils.getTimer();
				
				ElapsedTime = CurrentTime - PreviousTime;
				
				//PreviousTime = holdertime;
				
				CueTimer += ElapsedTime;
				
				for(var x:int = 0; x < CueVector.length; x++)
				{
					if(CueTimer > CueVector[x])
					{
						if(WordBoxes.length > x)
						{
							WordBoxes[x].color = 0xFFFFFF;
						}
						if(x > 0)
						{
							WordBoxes[x-1].color = 0x000000;
						}
					}
				}
				if(CueTimer >= CueVector[CueVector.length-1])
				{
					trace("Stop Sound");
					NarrationChannel.stop();
					NarrationDone = true;
				}
			}
		}
		
		public function InitializeTimer():void
		{
			CueTimer += CueVector[0];
			CurrentTime = flash.utils.getTimer();	
			PreviousTime = flash.utils.getTimer();	
		}
		
		public function LoadWords():void
		{
			wordsArray = Root.NarrationXML.Page[pagenum].PageText.split(" ");
			words = new <String>[];
			
			for (var i:int = 0; i < wordsArray.length; i++)
			{
				words.push(wordsArray[i] + " ");
				assets.enqueue(
					appDir.resolvePath("audio/IndividualWords/" + words[i].split(" ").join("").split(".").join("").split(",").join("").split("!").join("").split("-").join("").split("\"").join("").split(":").join("").toUpperCase() + ".mp3")
				);
				trace("Word: " + wordsArray[i]);
				//NarrationSounds.push(assets.getSound(wordsArray[i]));
			}
		}
		
		public function PreviousPage(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch == null){return;}
			trace("GoBack");
			//var pnt:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.BEGAN)
			{
				dispatchEventWith(PageManager.REVERSE_PAGE, true);
			}
		}
		
		public function NextPage(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch == null){return;}
			trace("Go Forward");
			//var pnt:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.BEGAN)
			{
				dispatchEventWith(PageManager.ADVANCE_PAGE, true);
			}
		}
		
		public function Pause():void
		{
			NarrationPausePoint = NarrationChannel.position;
			NarrationChannel.stop();
			trace("Stop");
			paused = true;
		}
		public function Resume():void
		{
			InitializeTimer();
			trace("Resume");
			NarrationChannel = Root.NarrationSound.play(NarrationPausePoint);
			if(PageManager.muted)
			{
				var sSound:SoundTransform = new SoundTransform();
				sSound.volume = 0;
				MuteButton.removeChildren();
				MuteButton.addChild(Mute1IMG);
				NarrationChannel.soundTransform = sSound;
			}
			else
			{
				NarrationChannel.soundTransform.volume = Costanza.voiceVolume;
			}
			paused = false;
		}
		
		public function stop():void
		{
			for(var x:int = 0; x < wordsArray.length; x++)
			{
				WordBoxes[x].color = 0x000000;

			}
			TweenMax.to(BackButton, 0.5, {alpha:0});
			TweenMax.to(ForwardButton, 0.5, {alpha:0});
			TweenMax.to(MenuButton, 0.5, {alpha:0});
			NarrationChannel.stop();
			removeEventListener(Event.ENTER_FRAME, cuePointTimer);
			BackButton.removeEventListener(TouchEvent.TOUCH, NextPage);
			ForwardButton.removeEventListener(TouchEvent.TOUCH, PreviousPage);
		}
		
		public function Destroy():void
		{
			//TweenMax.killAll();
			if(assets != null)
			{
				assets.dispose();
			}
			Starling.juggler.purge();
			NarrationChannel.stop();
			sceneRoot.removeChildren();
		}
		
		public function BeginNarration():void
		{
			ShowArrows();
			
			if(!PageManager.muted)
			{
				InitializeTimer();
				
				addEventListener(Event.ENTER_FRAME, cuePointTimer);
			
				NarrationChannel = Root.NarrationSound.play(CueVector[0],0);
			}
		}
		
		public function ShowArrows():void
		{
			BackButton.addEventListener(TouchEvent.TOUCH, NextPage);
			
			ForwardButton.addEventListener(TouchEvent.TOUCH, PreviousPage);
			
			MenuButton.addEventListener(TouchEvent.TOUCH, ToMenu);
			
			MuteButton.addEventListener(TouchEvent.TOUCH, MuteNarration);
			
			TweenMax.to(MenuButton, 1, {alpha:1});
			TweenMax.to(BackButton, 1, {alpha:1});
			TweenMax.to(ForwardButton, 1, {alpha:1});
		}
		
		public function ToMenu(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch == null){return;}
			//var pnt:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.BEGAN)
			{
				Destroy();
				dispatchEventWith(Root.LOAD_SUBMENU, true);
			}
		}
		
		public function MuteNarration(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch == null){return;}
			//var pnt:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(!PageManager.muted)
				{
					PageManager.muted = true;
					MuteButton.removeChildren();
					MuteButton.addChild(Mute1IMG);
					NarrationChannel.stop();
					NarrationDone = true;
					for(var x:int = 0; x < wordsArray.length; x++)
					{
						WordBoxes[x].color = 0x000000;
					}
					//removeEventListener(Event.ENTER_FRAME, cuePointTimer);
				}
				else
				{
					NarrationDone = false;
					PageManager.muted = false;
					MuteButton.removeChildren();
					MuteButton.addChild(Mute2IMG);
					
					CueTimer = 0;
					BeginNarration();
					//addEventListener(Event.ENTER_FRAME, cuePointTimer);
					//InitializeTimer();
				}
			}
		}
	}
}

