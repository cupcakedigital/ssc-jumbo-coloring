package ui
{
	import flash.filesystem.File;
	import flash.system.Capabilities;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;

	public class Help extends Sprite
	{
		private var helpAssets:AssetManager;
		private var helpAppDir:File = File.applicationDirectory;
		private var soundmanager:SoundManager;
		
		private var HelpImg:Image;
		private var CreditsImg:Image;
		
		public function Help()
		{
			helpAssets = new AssetManager();
			helpAssets.verbose = Capabilities.isDebugger;
			
			helpAssets.enqueue(
				helpAppDir.resolvePath("textures/Help/Credits.png"),
				helpAppDir.resolvePath("textures/Help/Help.png")
			);
			
			//dispatchEventWith(HIDE_COMIC,true);
			
			helpAssets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					addObjects();
				};
			});
		}
		
		private function addObjects():void
		{
			HelpImg = new Image(helpAssets.getTexture("Help"));
			this.addChild(HelpImg);
			
			var creditsBTN:Quad = new Quad(150,100);
			creditsBTN.alpha = 0;
			creditsBTN.x = 980;
			creditsBTN.y = HelpImg.y + HelpImg.height - creditsBTN.height;
			this.addChild(creditsBTN);
			creditsBTN.addEventListener(TouchEvent.TOUCH, showCredits);
			
			CreditsImg = new Image(helpAssets.getTexture("Credits"));
			this.addChild(CreditsImg);
			CreditsImg.visible = false;
			CreditsImg.touchable = false;
			
			var closeBTN:Quad = new Quad(100,100);
			closeBTN.alpha = 0;
			closeBTN.x = 1040;
			closeBTN.y = HelpImg.y + 50;
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);
			
			/*skip = new Button(comicAssets.getTexture("Back_1"),"",comicAssets.getTexture("Back_2"));
			skip.x = 1000;
			skip.y = 660;
			skip.touchable = skip.visible = showskip;
			this.addChild(skip);
			skip.addEventListener(Event.TRIGGERED,skipComic);*/
		}
		
		private function showCredits(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				CreditsImg.visible = true;
				CreditsImg.touchable = true;
			}
		}
		
		private function closeTap(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(CreditsImg.visible)
				{
					CreditsImg.visible = false;
					CreditsImg.touchable = false;
				}
				else
				{
					//goBack
					destroy();
				}
			}
		}
		
		public function destroy():void
		{
			helpAssets.dispose();
			//soundManager.removeSound("WidgetTheme");
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}