package ui
{
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.AssetManager;

	public class Settings extends Sprite
	{
		public static const HIDE_SETTINGS:String = "hideSettings";
		public static const UPDATE_MUSIC:String = "updatemusic";
		
		private var musicSlider:Image;
		private var soundSlider:Image;
		private var voiceSlider:Image;
		
		private var dragging:Boolean;
		
		private var startX:Number = 175;//min point for slider
		private var endX:Number = 600;//max point for slider
		private var barWidth:Number = endX-startX;

		private var ok_BTN:Quad;
		private var ok_Text:TextField;
		
		private var bg:Image;
		
		public function Settings( assets:AssetManager )
		{
			bg = new Image(assets.getTexture("Settings_BG"));
			bg.x = 0;
			bg.y = 0;
			this.addChild(bg);
			
			musicSlider = new Image(assets.getTexture("Slider"));
			musicSlider.pivotX = musicSlider.width/2;
			musicSlider.x = startX + (barWidth*(Costanza.musicVolume/.4)) - musicSlider.width/2;
			musicSlider.y = 133;
			musicSlider.name = "music";
			
			soundSlider = new Image(assets.getTexture("Slider"));
			soundSlider.pivotX = soundSlider.width/2;
			soundSlider.x = startX + (barWidth*Costanza.soundVolume) - soundSlider.width/2;;
			soundSlider.y = musicSlider.y + musicSlider.height + 44;
			soundSlider.name = "sound";
			
			voiceSlider = new Image(assets.getTexture("Slider"));
			voiceSlider.pivotX = voiceSlider.width/2;
			voiceSlider.x = startX + (barWidth*Costanza.voiceVolume) - voiceSlider.width/2;;
			voiceSlider.y = soundSlider.y + soundSlider.height + 44;
			voiceSlider.name = "voice";
			
			this.addChild(musicSlider);
			this.addChild(soundSlider);
			this.addChild(voiceSlider);

			//ok button
			ok_BTN = new Quad(250,80);//Image(assets.getTexture("Dialog_Button_Normal_s"));
			ok_BTN.x = this.width/2 - ok_BTN.width/2;
			ok_BTN.y = this.height - 75;
			ok_BTN.alpha = 0;
			this.addChild(ok_BTN);
			ok_BTN.addEventListener(TouchEvent.TOUCH, closeSettings);
			
			//text for button
			/*ok_Text = new TextField(200,150, "OK", "Tastic", 60);
			ok_Text.color = Color.WHITE;
			ok_Text.x = ok_BTN.x - 10;
			ok_Text.y = ok_BTN.y -30;
			this.addChild(ok_Text);
			ok_Text.touchable = false;*/
			
			//bg.addEventListener(TouchEvent.TOUCH,dragSlider);
			musicSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			soundSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			voiceSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			
		}//end constructor
		
		private function closeSettings(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				trace("hide");
				trace(SSC_Coloring.savedVariablesObject.data.soundVolume);
				//saves values to sharedobject
				SSC_Coloring.savedVariablesObject.data.musicVolume = Costanza.musicVolume;
				SSC_Coloring.savedVariablesObject.data.voiceVolume = Costanza.voiceVolume;
				SSC_Coloring.savedVariablesObject.data.soundVolume = Costanza.soundVolume;
				
				SSC_Coloring.savedVariablesObject.flush();
				
				dispatchEventWith(HIDE_SETTINGS,true);
				trace(SSC_Coloring.savedVariablesObject.data.soundVolume);
			}
		}
		
		private function dragSlider(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			var mc:Image = e.currentTarget as Image;
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				trace(touch.globalX);
				dragging = true;
			}
			else if(touch.phase == TouchPhase.MOVED)
			{
				if(dragging)
				{
					//mc.x = touch.globalX - this.x - musicSlider.width/2;
					//trace(touch.globalX);
					mc.x = ((touch.globalX - (this.width / 2)) - (musicSlider.width / 2)) - Costanza.STAGE_OFFSET;					
					if(mc.x > endX){mc.x = endX}
					if(mc.x < startX){mc.x = startX}
					setVolume(mc);
				}
			}
			else if(touch.phase == TouchPhase.ENDED)
			{
				dragging = false;
				setVolume(mc);
			}
			
						
			
		}//end dragSlider
		
		private function setVolume(mc:Image):void
		{
			switch(mc.name)
			{
				case "music":
				{
					Costanza.musicVolume = ((mc.x - startX)/barWidth)*.4;
					trace(mc.x - startX);
					if(mc.x == startX){Costanza.musicVolume=0;}
					dispatchEventWith(UPDATE_MUSIC, true);				
					break;
				}
				case "sound":
				{
					Costanza.soundVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.soundVolume=0;}
					break;
				}
				case "voice":
				{
					Costanza.voiceVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.voiceVolume=0;}
					break;
				}
			}
		}
	}
}