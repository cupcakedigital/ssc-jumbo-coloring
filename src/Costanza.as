package{
	import com.cupcake.Utils;
	
	import feathers.text.BitmapFontTextFormat;
	
	import starling.errors.AbstractClassError;

    public class Costanza{
		
        public function Costanza(){throw new AbstractClassError();}
		
		public static const DEV_KEY:String 			= "ca35e2c84f5cabd51e942026ab0036be2909cd1bQVeGNamSskCBQkvmer/nVbAzlZiJCWvyNgdPOGeianK6umpBGZ/+KmQLBGaKWUQ292/ykDEzWziKSBJtsbhL1w==";
		
		public static const NABI_OFFSET:int				= 20;
		public static const NABI_COLORING_OFFSET:int	= 30;
		
		//Lable format.
		public static var LABEL_TEXT_FORMAT:BitmapFontTextFormat;
        
		//Stage itself.
		public static var STAGE_WIDTH:int  				= 1366;
		public static var STAGE_HEIGHT:int 				= 768;
		public static var STAGE_OFFSET:int 				= 0;
		public static var SCALE_FACTOR:int 				= 1;	
		
		//Visible regions.
		public static var VIEWPORT_WIDTH:int  			= 1024;
		public static var VIEWPORT_HEIGHT:int 			= 768;
		public static var VIEWPORT_OFFSET:int 			= 171;	
		
		//Retna.
		public static var IPAD_RETINA:Boolean 			= false;
		public static var RETINA_WIDTH:int;
		public static var RETINA_HEIGHT:int;
		
		public static var mipmapsEnabled:Boolean 		= false;

		//sound variables
		public static var musicVolume:Number 			= .4;
		public static var voiceVolume:Number 			= 1;
		public static var soundVolume:Number 			= 1;	
		
		//App variables.
		public static var Test_Retna:Boolean 			= false;
		public static var playedOnce:Boolean 			= false;
		public static var Tutorial_Completed:Boolean 	= true;	
		public static var StageProportionScale:Number   = 1.0;
		
		public static const VIDEO_FILES:String			= "Cupcake_Splash,Splash_Screen_Video";
		public static const VIDEO_WIDTHS:String			= "1366,1024";
		
		CONFIG::FREE
		{
			public static const MENU_LEFT:int			= -300;
			public static const PACK_NAMES:String		= "Friends,Ballet,Pets,Princess,Winter";
			public static const PACK_IDS:String			= "friends_pack,ballet_pack,pets_pack,princess_pack,winter_pack"
			public static const PACK_LABELS:String		= "BBF,Ballerina,Pets,Princess,Winter";
			public static const PACK_SOUNDS:String		= "friends,ballet,ballet,princess,winter";
			public static const PACK_SONGS:String		= "1,2,3,4,5";
			public static const RELEASE:String			= "free";
		}
		
		CONFIG::FULL
		{
			public static const MENU_LEFT:int			= -800;
			public static const PACK_NAMES:String		= "Summer,Friends,Ballet,Pets,Princess,Winter,Easter";
			public static const PACK_IDS:String			= "summer_pack,friends_pack,ballet_pack,pets_pack,princess_pack,winter_pack,easter_pack";
			public static const PACK_LABELS:String		= "Summer,BBF,Ballerina,Pets,Princess,Winter,Easter";
			public static const PACK_SOUNDS:String		= "friends,friends,ballet,ballet,princess,winter,winter";
			public static const PACK_SONGS:String		= "6,1,2,3,4,5,4";
			public static const RELEASE:String			= "full";
		}
		
		
		public static var packNames:Vector.<String>;
		public static var packIDs:Vector.<String>;
		public static var packLabels:Vector.<String>;
		public static var packSongs:Vector.<String>;
		public static var packSounds:Vector.<String>;
		
		{
			packNames = Utils.CreateStringVector( PACK_NAMES );
			packLabels = Utils.CreateStringVector( PACK_LABELS );
			packIDs = Utils.CreateStringVector( PACK_IDS );
			packSounds = Utils.CreateStringVector( PACK_SOUNDS );
			packSongs = Utils.CreateStringVector( PACK_SONGS );
		}

    }
}