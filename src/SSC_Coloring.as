package{
	import com.cupcake.App;
    import com.cupcake.DeviceInfo;

    import com.greensock.TweenMax;
    import com.sickle.flickbackFlash.FlickbackConnect;
	
	CONFIG::PUSH
	{
		import com.pushwoosh.nativeExtensions.PushNotification;
		import com.pushwoosh.nativeExtensions.PushNotificationEvent;
	}	
    
    import flash.desktop.NativeApplication;
    import flash.desktop.SystemIdleMode;
    import flash.display.Bitmap;
    import flash.display.Sprite;
    import flash.display3D.Context3DProfile;
    import flash.display3D.Context3DRenderMode;
    import flash.events.Event;
    import flash.filesystem.File;
    import flash.geom.Rectangle;
    import flash.net.SharedObject;
    import flash.system.Capabilities;
    import flash.text.Font;
    
    import starling.core.Starling;
    import starling.events.Event;
    import starling.extensions.SoundManager;
    import starling.textures.Texture;
    import starling.utils.AssetManager;
    import starling.utils.RectangleUtil;
    import starling.utils.ScaleMode;
	
	import utils.Captcha;

    [SWF(frameRate="60", backgroundColor="0x000000")]	
	
	//This is a coloring book app.
    public class SSC_Coloring extends Sprite{
		
        //Embed Background Loadup Image.
        //[Embed(source="/startup.png")]
		[Embed(source="/loading 1x.png")]
        private static var Background:Class;
		
		//[Embed(source="/startupHD.png")]
		[Embed(source="/loading 2x.png")]
		private static var BackgroundHD:Class;

		//Embed Fonts.
		[Embed(	source 					= "../assets/fonts/FRUITASTIC.ttf",
				fontName 				= "Tastic",
				fontFamily				= "Fruit",
				mimeType 				= "application/x-font",
				advancedAntiAliasing 	= "true",
				embedAsCFF				= "false")]
		public static const FruitTasticFONT:Class;	
		
		//Embed Fonts.
		[Embed(	source 					= "../assets/fonts/Noteworthy.ttc",
				fontName 				= "noteworthy",
				fontFamily				= "NOTE",
				mimeType 				= "application/x-font",
				advancedAntiAliasing 	= "true",
				embedAsCFF				= "false")]
		public static const NoteworthyFont:Class;//for more apps	
		
		//Starling.
		private var mStarling:Starling;
		private var mStarlingProfile:String;
		
		//Sounds.
		private var soundManager:SoundManager;
		
		//Header font.
		public static var headerFont:Font;
		
		//Saved Data.
		public static var savedVariablesObject:SharedObject;
		
		//AmazonPurchase blackscreen fix
		private static var flickFix:Boolean = false;
		
		//Creation.
        public function SSC_Coloring(){	
			App.Setup( this );
			
			
			//register for push notifications
			CONFIG::PUSH
			{
				var pushwoosh:PushNotification = PushNotification.getInstance();
				pushwoosh.addEventListener(PushNotificationEvent.PERMISSION_GIVEN_WITH_TOKEN_EVENT, onToken);
				pushwoosh.addEventListener(PushNotificationEvent.PERMISSION_REFUSED_EVENT, onError);
				pushwoosh.addEventListener(PushNotificationEvent.PUSH_NOTIFICATION_RECEIVED_EVENT, onPushReceived);
				pushwoosh.registerForPushNotification();
			}
            
			//Set profile string.
			mStarlingProfile 			= Context3DProfile.BASELINE;
			Costanza.Test_Retna			= false;
			//Setup if Retna specs.
			if(Costanza.Test_Retna || (Capabilities.screenResolutionX == 1536 && Capabilities.screenResolutionY == 2048 && CONFIG::MARKET == "itunes")){
				Costanza.IPAD_RETINA 	= true;
				Costanza.STAGE_HEIGHT 	= 768;
				Costanza.STAGE_WIDTH 	= 1024;
				Costanza.SCALE_FACTOR 	= 2;
				Costanza.STAGE_OFFSET 	= -171;
				mStarlingProfile 		= Context3DProfile.BASELINE_EXTENDED;
			}			

			//Setup Header Fonts.
			headerFont = new FruitTasticFONT();
			Font.registerFont(FruitTasticFONT);
			
			//No Background executions.
			NativeApplication.nativeApplication.executeInBackground = false;

			//Setup stage and sizing params. also device detect for context.
			//stage.align 				= StageAlign.TOP;
			
            var stageWidth:int   		= Costanza.STAGE_WIDTH;
            var stageHeight:int  		= Costanza.STAGE_HEIGHT;
           	
			Costanza.VIEWPORT_WIDTH 	= stage.fullScreenWidth;
			Costanza.VIEWPORT_HEIGHT 	= stage.fullScreenHeight - ( CONFIG::MARKET == "nabi" ? Costanza.NABI_OFFSET : 0 );
			Costanza.VIEWPORT_OFFSET 	= (Costanza.STAGE_WIDTH - Costanza.VIEWPORT_WIDTH) / 2;
			trace(Costanza.VIEWPORT_OFFSET);
			var viewPort:Rectangle 		= RectangleUtil.fit(new Rectangle(0, 0, stageWidth, stageHeight), 
															new Rectangle(0, 0, Costanza.VIEWPORT_WIDTH , Costanza.VIEWPORT_HEIGHT), 
															ScaleMode.NO_BORDER);			
			
			if ( CONFIG::MARKET == "nabi" ) { viewPort.top = 0; }
			var iOS:Boolean 			= Capabilities.manufacturer.indexOf("iOS") != -1;	
            Starling.multitouchEnabled 	= true;  // useful on mobile devices
            Starling.handleLostContext 	= !iOS;  // not necessary on iOS. Saves a lot of memory!
			
			//check resolution to see if we need mipmapping enabled
			if(stage.fullScreenHeight < 384){Costanza.mipmapsEnabled = true;}

			//shared Object with stored variables
			savedVariablesObject = SharedObject.getLocal("SSCSAVEVAR");
			setSharedData();		
			
			//Q asset manager.
			var appDir:File 			= File.applicationDirectory;		 
            var assets:AssetManager 	= new AssetManager(Costanza.SCALE_FACTOR);           
			trace("Loading with scale factor of: " + Costanza.SCALE_FACTOR);	
            assets.verbose = Capabilities.isDebugger;
            assets.enqueue(
                appDir.resolvePath("audio/MenuSFX"),
				appDir.resolvePath("particles"),			
                appDir.resolvePath("fonts")
            );
            
            //Setup Background.
			var backgroundClass:Class = Costanza.SCALE_FACTOR == 1 ? Background : BackgroundHD;
			var background:Bitmap = new backgroundClass();
			BackgroundHD = null; // no longer needed!
			Background = null; // no longer needed!			
			
			trace(background.width + " " + background.height);
			
            //Size.
            background.x 				= stageWidth / 2 - background.width / 2 - Costanza.VIEWPORT_OFFSET;
            background.y 				= stageHeight / 2 - background.height / 2;
            background.smoothing 		= true;
			background.visible			= CONFIG::MARKET == "itunes";
            addChild(background);		//Populate.  
			

			//Setup Starling.          
			mStarling 						= new Starling(Root, stage, viewPort, null, Context3DRenderMode.AUTO, mStarlingProfile);
            mStarling.stage.stageWidth  	= stageWidth;  // <- same size on all devices!
            mStarling.stage.stageHeight 	= stageHeight; // <- same size on all devices!
            mStarling.simulateMultitouch  	= false;
            mStarling.enableErrorChecking 	= Capabilities.isDebugger;	
			
			//Create sound manager.
			soundManager 					= SoundManager.getInstance();
			
			//Setup activation / deactivation listeners.
			NativeApplication.nativeApplication.addEventListener( flash.events.Event.ACTIVATE, WindowActivated ); 
			NativeApplication.nativeApplication.addEventListener( flash.events.Event.DEACTIVATE, WindowDeactivated );
			
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && CONFIG::MARKET != "nabi" ) 
				{ stage.addEventListener( flash.events.Event.RESIZE, StageResized ); }
			
			//Create the Root script now.
            mStarling.addEventListener(starling.events.Event.ROOT_CREATED, 
                function onRootCreated(event:Object, app:Root):void{
					trace( "Root created!" );
					//Remove the listener for creation.
                    mStarling.removeEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
					//Remove Pre splash Splash.
                    removeChild(background);//and Clean.
                    background 				= null;
                    
					//Create Background Splash.
                    var bgTexture:Texture 	= Texture.fromEmbeddedAsset(backgroundClass, false, false, Costanza.SCALE_FACTOR);
					//Start Root with a background and assets to load.
                    app.start(bgTexture, assets);
					//Begin Starling as Root for scene.
                    mStarling.start();
					
            });
			
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID ) { FlickbackConnect.init(); }
        	TweenMax.delayedCall( 2, ResetFlickFix );
        }
		
		private function WindowActivated( e:flash.events.Event ):void
		{ 
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && !CONFIG::NOOK ) 
				{ NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE; }

			if ( !Captcha.visible )
			{
				mStarling.start(); 
				mStarling.render();
			}
			
			soundManager.muteAll(false); 
			TweenMax.resumeAll(); 
			if ( stage ) { stage.quality = stage.quality; } 
			if ( flickFix && DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID )
			{
				flickFix = false;
				FlickbackConnect.showFlickScreen();
				TweenMax.delayedCall( 2, ResetFlickFix );
			}
			trace( "Window activated" );
		}
		
		private function ResetFlickFix():void { flickFix = true; }
		
		private function WindowDeactivated( e:flash.events.Event ):void
		{ 
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && !CONFIG::NOOK ) 
				{ NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL; }
			
			mStarling.stop(); 
			soundManager.muteAll(true); 
			TweenMax.pauseAll();
			trace( "Window deactivated" );
		}
		
		private function StageResized( e:flash.events.Event ):void
		{
			trace( "StageResized" );
			Costanza.VIEWPORT_WIDTH = stage.stageWidth;
			Costanza.VIEWPORT_HEIGHT = stage.stageHeight;
			Costanza.VIEWPORT_OFFSET = (Costanza.STAGE_WIDTH - Costanza.VIEWPORT_WIDTH)/2;
			
			var viewPort:Rectangle = RectangleUtil.fit(
				new Rectangle(0, 0, Costanza.STAGE_WIDTH, Costanza.STAGE_HEIGHT), 
				new Rectangle(0, 0, Costanza.VIEWPORT_WIDTH , Costanza.VIEWPORT_HEIGHT), 
				ScaleMode.NO_BORDER);
			
			Starling.current.viewPort = viewPort;
			
			// only do this once on nook
			if ( CONFIG::MARKET == "nook" ) { stage.removeEventListener( flash.events.Event.RESIZE, StageResized ); }
		}
		
		private function setSharedData():void{	
			
			//volume.
			(savedVariablesObject.data.musicVolume >= 0) ? Costanza.musicVolume = savedVariablesObject.data.musicVolume : savedVariablesObject.data.musicVolume = Costanza.musicVolume;
			(savedVariablesObject.data.voiceVolume >= 0) ? Costanza.voiceVolume = savedVariablesObject.data.voiceVolume : savedVariablesObject.data.voiceVolume = Costanza.voiceVolume;
			(savedVariablesObject.data.soundVolume >= 0) ? Costanza.soundVolume = savedVariablesObject.data.soundVolume : savedVariablesObject.data.soundVolume = Costanza.soundVolume;
			
			//Save data.
			savedVariablesObject.flush();
			
		}
		
		CONFIG::PUSH
		{
			public static function onToken(e:PushNotificationEvent):void
			{ trace("\n TOKEN: " + e.token + " "); }
			
			public static function onError(e:PushNotificationEvent):void
			{ trace("\n TOKEN: " + e.errorMessage+ " "); }
			
			public static function onPushReceived(e:PushNotificationEvent):void
			{ trace("\n TOKEN: " + JSON.stringify(e.parameters) + " "); }
		}
    }
}