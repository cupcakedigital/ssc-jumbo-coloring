package scenes
{	
	import com.cupcake.IAPManager;
	
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.system.Capabilities;
	import flash.system.System;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.filters.BlurFilter;
	import starling.text.TextField;
	import starling.utils.AssetManager;
	
	import utils.ButtonAlpha;
	import utils.Captcha;
	
	public class Pack_Selection extends starling.display.Sprite
	{
		//Core.
		private 	   var soundManager:SoundManager;
		private static var assets:AssetManager;
		private 	   var appDir:File;
		private        var assetsLoaded:Boolean;	
		
		// Scene Objects
		private var sceneRoot:starling.display.Sprite;

		private var bg:Image;
		
		private var Captcha_Holder:flash.display.Sprite;
		private var Return_Button:ButtonAlpha;		
		private var UpSell_Deal_Selection_BTN:ButtonAlpha;
		private var Pack_1_Selection_BTN:ButtonAlpha;
		private var Pack_3_Selection_BTN:ButtonAlpha;
		private var Pack_4_Selection_BTN:ButtonAlpha;
		private var Pack_5_Selection_BTN:ButtonAlpha;
		
		private var Pack_1_Selection_Buy_Image:Image;
		private var Pack_3_Selection_Buy_Image:Image;
		private var Pack_4_Selection_Buy_Image:Image;
		private var Pack_5_Selection_Buy_Image:Image;
		
		private var pack1Price:TextField;
		private var pack3Price:TextField;
		private var pack4Price:TextField;
		private var pack5Price:TextField;
		private var bundlePrice:TextField;
		
		private var closing:Boolean			= false;
		private var purchasing:Boolean		= false;
		private var targetPack:String		= "";
		
		public static const TEXT_HEIGHT:int		= 30;
		public static const TEXT_WIDTH:int		= 100;

		public function Pack_Selection(charString:String = ""){
			
			//Setup More Apps Holder.
			Captcha_Holder 			= new flash.display.Sprite();
			Captcha_Holder.x 		= (171 + Costanza.STAGE_OFFSET);
			
			assetsLoaded 			= false;
			appDir 					= File.applicationDirectory;			
			
			assets 					= new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps 		= Costanza.mipmapsEnabled;
			assets.verbose 			= Capabilities.isDebugger;
			
			assets.enqueue(
				appDir.resolvePath("textures/upsell/x" + Costanza.SCALE_FACTOR.toString() + "/")			
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void{				
				if(ratio == 1){
					Starling.juggler.delayCall(function():void{						
						assetsLoaded = true;							
						Build_Scene();
					}, 0.15);
					System.pauseForGCIfCollectionImminent(0);
					System.gc();	
				};
			});		
		}	
		
		
		private function Build_Scene():void{
			
			// Create main scene root
			sceneRoot = new starling.display.Sprite();
			this.addChild(sceneRoot);
			
			//Instance soundmanager and add objects
			soundManager = SoundManager.getInstance();
			
			//Background.
			bg = new Image(assets.getTexture("UpSell_Background"));
			sceneRoot.addChild(bg);		
			
			//The Deal.
			UpSell_Deal_Selection_BTN					= new ButtonAlpha(assets.getTexture("UpSell_Deal_Selection"));
			UpSell_Deal_Selection_BTN.addEventListener(starling.events.Event.TRIGGERED, Buy_Bundle_Deal);
			UpSell_Deal_Selection_BTN.x 				= (1366 / 2) - (UpSell_Deal_Selection_BTN.width / 2);
			UpSell_Deal_Selection_BTN.y 				= 5;
			UpSell_Deal_Selection_BTN.touchable 		= true;		
			UpSell_Deal_Selection_BTN.scaleWhenDown 	= 1.0;
			sceneRoot.addChild(UpSell_Deal_Selection_BTN);
			
			bundlePrice = new TextField( TEXT_WIDTH, TEXT_HEIGHT, IAPManager.GetProduct( "bundle_pack" ).price, "_sans", 24, 0xFFFFFF, true );
			bundlePrice.x = UpSell_Deal_Selection_BTN.x + 475;
			bundlePrice.y = UpSell_Deal_Selection_BTN.y + 265;
			sceneRoot.addChild(bundlePrice);
			
			Pack_1_Selection_BTN				= new ButtonAlpha(assets.getTexture("UpSell_Ballerina_Selection"));
			Pack_1_Selection_BTN.addEventListener(starling.events.Event.TRIGGERED, Buy_Pack_1);
			Pack_1_Selection_BTN.x 				= 171;
			Pack_1_Selection_BTN.y 				= 350;
			Pack_1_Selection_BTN.touchable 		= true;		
			Pack_1_Selection_BTN.scaleWhenDown 	= 1.0;
			
			if(!IAPManager.IsPurchased( "ballet_pack" )){
				Pack_1_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Purchase_Icon"));
				Pack_1_Selection_Buy_Image.name = "buy";
			}else{
				Pack_1_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Play_Icon"));
				Pack_1_Selection_Buy_Image.name = "play";
			}
			
			
			Pack_1_Selection_Buy_Image.x 		= (Pack_1_Selection_BTN.x + 15);
			Pack_1_Selection_Buy_Image.y 		= (Pack_1_Selection_BTN.y + 325);
			pack1Price							= new TextField( TEXT_WIDTH, TEXT_HEIGHT, IAPManager.GetProduct( "ballet_pack" ).price, "_sans", 24, 0xFFFFFF, true );
			pack1Price.hAlign					= "center";
			pack1Price.x						= (Pack_1_Selection_Buy_Image.x + 90);
			pack1Price.y						= (Pack_1_Selection_Buy_Image.y + 40);
			pack1Price.visible					= !IAPManager.IsPurchased( "ballet_pack" );
			Pack_1_Selection_Buy_Image.touchable = false;
			
			sceneRoot.addChild(Pack_1_Selection_BTN);
			sceneRoot.addChild(Pack_1_Selection_Buy_Image);
			sceneRoot.addChild(pack1Price);
			
			Pack_3_Selection_BTN				= new ButtonAlpha(assets.getTexture("UpSell_Pets_Selection"));
			Pack_3_Selection_BTN.addEventListener(starling.events.Event.TRIGGERED, Buy_Pack_3);
			Pack_3_Selection_BTN.x 				= (171 + (1 * (Pack_3_Selection_BTN.width - 15))) + (1 * 17);
			Pack_3_Selection_BTN.y 				= 350;
			Pack_3_Selection_BTN.touchable 		= true;		
			Pack_3_Selection_BTN.scaleWhenDown 	= 1.0;
			
			if(!IAPManager.IsPurchased( "pets_pack" )){
				Pack_3_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Purchase_Icon"));
				Pack_3_Selection_Buy_Image.name = "buy";
			}else{
				Pack_3_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Play_Icon"));
				Pack_3_Selection_Buy_Image.name = "play";
			}
			
			Pack_3_Selection_Buy_Image.x 		= (Pack_3_Selection_BTN.x + 15);
			Pack_3_Selection_Buy_Image.y 		= (Pack_3_Selection_BTN.y + 325);
			pack3Price							= new TextField( TEXT_WIDTH, TEXT_HEIGHT, IAPManager.GetProduct( "pets_pack" ).price, "_sans", 24, 0xFFFFFF, true );
			pack3Price.x						= (Pack_3_Selection_Buy_Image.x + 90);
			pack3Price.y						= (Pack_3_Selection_Buy_Image.y + 40);
			pack3Price.visible					= !IAPManager.IsPurchased( "pets_pack" );
			Pack_3_Selection_Buy_Image.touchable = false;
			
			sceneRoot.addChild(Pack_3_Selection_BTN);
			sceneRoot.addChild(Pack_3_Selection_Buy_Image);	
			sceneRoot.addChild(pack3Price);
			
			Pack_4_Selection_BTN				= new ButtonAlpha(assets.getTexture("UpSell_Princess_Selection"));
			Pack_4_Selection_BTN.addEventListener(starling.events.Event.TRIGGERED, Buy_Pack_4);
			Pack_4_Selection_BTN.x 				= (171 + (2 * (Pack_4_Selection_BTN.width - 15))) + (2 * 17);
			Pack_4_Selection_BTN.y 				= 350;
			Pack_4_Selection_BTN.touchable 		= true;		
			Pack_4_Selection_BTN.scaleWhenDown 	= 1.0;
			
			if(!IAPManager.IsPurchased( "princess_pack" )){
				Pack_4_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Purchase_Icon"));
				Pack_4_Selection_Buy_Image.name = "buy";
			}else{
				Pack_4_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Play_Icon"));
				Pack_4_Selection_Buy_Image.name = "play";
			}
			
			trace( "Princess pack cost: " + IAPManager.GetProduct( "princess_pack" ).price );
			Pack_4_Selection_Buy_Image.x 		= (Pack_4_Selection_BTN.x + 15);
			Pack_4_Selection_Buy_Image.y 		= (Pack_4_Selection_BTN.y + 325);
			pack4Price							= new TextField( TEXT_WIDTH, TEXT_HEIGHT, IAPManager.GetProduct( "princess_pack" ).price, "_sans", 24, 0xFFFFFF, true );
			pack4Price.x						= (Pack_4_Selection_Buy_Image.x + 90);
			pack4Price.y						= (Pack_4_Selection_Buy_Image.y + 40);
			pack4Price.visible					= !IAPManager.IsPurchased( "princess_pack" );
			Pack_4_Selection_Buy_Image.touchable = false;
			
			sceneRoot.addChild(Pack_4_Selection_BTN);
			sceneRoot.addChild(Pack_4_Selection_Buy_Image);
			sceneRoot.addChild(pack4Price);
			
			
			Pack_5_Selection_BTN				= new ButtonAlpha(assets.getTexture("UpSell_Winter_Selection"));
			Pack_5_Selection_BTN.addEventListener(starling.events.Event.TRIGGERED, Buy_Pack_5);
			Pack_5_Selection_BTN.x 				= (171 + (3 * (Pack_5_Selection_BTN.width - 15))) + (3 * 17);
			Pack_5_Selection_BTN.y 				= 350;
			Pack_5_Selection_BTN.touchable 		= true;		
			Pack_5_Selection_BTN.scaleWhenDown 	= 1.0;
			
			if(!IAPManager.IsPurchased( "winter_pack" )){
				Pack_5_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Purchase_Icon"));
				Pack_5_Selection_Buy_Image.name = "buy";
			}else{
				Pack_5_Selection_Buy_Image 			= new Image(assets.getTexture("UpSell_Play_Icon"));	
				Pack_5_Selection_Buy_Image.name = "play";
			}
			
			Pack_5_Selection_Buy_Image.x 		= (Pack_5_Selection_BTN.x + 15);
			Pack_5_Selection_Buy_Image.y 		= (Pack_5_Selection_BTN.y + 325);
			pack5Price							= new TextField( TEXT_WIDTH, TEXT_HEIGHT, IAPManager.GetProduct( "winter_pack" ).price, "_sans", 24, 0xFFFFFF, true );
			pack5Price.x						= (Pack_5_Selection_Buy_Image.x + 90);
			pack5Price.y						= (Pack_5_Selection_Buy_Image.y + 40);
			pack5Price.visible					= !IAPManager.IsPurchased( "winter_pack" );
			Pack_5_Selection_Buy_Image.touchable = false;
			
			sceneRoot.addChild(Pack_5_Selection_BTN);
			sceneRoot.addChild(Pack_5_Selection_Buy_Image);
			sceneRoot.addChild(pack5Price);
			
			//Return.
			Return_Button					= new ButtonAlpha(assets.getTexture("xtc"));
			Return_Button.addEventListener(starling.events.Event.TRIGGERED, GoBack);
			Return_Button.x 				= ((1024 + 171) - (Return_Button.width) + 40);
			Return_Button.y 				= 0;
			Return_Button.touchable 		= true;	
			Return_Button.scaleX = .7;
			Return_Button.scaleY = .7;
			Return_Button.scaleWhenDown 	= 1;
			sceneRoot.addChild(Return_Button);		
			
			this.addEventListener(Root.DISPOSE,  disposeScene);	
			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, FocusGained);
			IAPManager.manifestUpdated.add( checkButtons );
			IAPManager.actionCompleted.add( PurchaseComplete );
			
			//Build Completed.
			dispatchEventWith(Root.SCENE_LOADED, true);
			
		}
		
		
		
		private function checkButtons():void
		{
			if(IAPManager.IsPurchased( "bundle_pack" ))
			{	
				ForceGoBack();
			}
			else
			{
				if ((IAPManager.IsPurchased( "winter_pack" )) && Pack_5_Selection_Buy_Image.name == "buy")
				{
					Pack_5_Selection_Buy_Image.texture.dispose();
					Pack_5_Selection_Buy_Image.texture = assets.getTexture("UpSell_Play_Icon");
					Pack_5_Selection_Buy_Image.readjustSize();
					pack5Price.visible = false;
				}
				if ((IAPManager.IsPurchased( "princess_pack" )) && Pack_4_Selection_Buy_Image.name == "buy")
				{
					Pack_4_Selection_Buy_Image.texture.dispose();
					Pack_4_Selection_Buy_Image.texture = assets.getTexture("UpSell_Play_Icon");
					Pack_4_Selection_Buy_Image.readjustSize();
					pack4Price.visible = false;
				}
				if ((IAPManager.IsPurchased( "pets_pack" )) && Pack_3_Selection_Buy_Image.name == "buy")
				{
					Pack_3_Selection_Buy_Image.texture.dispose();
					Pack_3_Selection_Buy_Image.texture = assets.getTexture("UpSell_Play_Icon");
					Pack_3_Selection_Buy_Image.readjustSize();
					pack3Price.visible = false;
				}
				if ((IAPManager.IsPurchased( "ballet_pack" )) && Pack_1_Selection_Buy_Image.name == "buy")
				{
					Pack_1_Selection_Buy_Image.texture.dispose();
					Pack_1_Selection_Buy_Image.texture = assets.getTexture("UpSell_Play_Icon");
					Pack_1_Selection_Buy_Image.readjustSize();
					pack1Price.visible = false;
				}
			}
		}
		
		private function disposeScene():void{
			
			trace("Dispose Pack Selection");
			
			this.removeEventListener(Root.DISPOSE, disposeScene);
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ACTIVATE, FocusGained);	
			
			//Clean Back BTN.
			Return_Button.removeEventListener(starling.events.Event.TRIGGERED, GoBack);
			sceneRoot.removeChild(Return_Button);
			Return_Button.dispose();
			
			//Clean Deal.
			if(UpSell_Deal_Selection_BTN != null){				
				UpSell_Deal_Selection_BTN.removeEventListener(starling.events.Event.TRIGGERED, Buy_Bundle_Deal);
				sceneRoot.removeChild(UpSell_Deal_Selection_BTN);
				UpSell_Deal_Selection_BTN.dispose();
			}
			
			if(Pack_1_Selection_BTN != null){
				sceneRoot.removeChild(Pack_1_Selection_Buy_Image);
				Pack_1_Selection_Buy_Image.dispose();
				Pack_1_Selection_BTN.removeEventListener(starling.events.Event.TRIGGERED, Buy_Pack_1);
				sceneRoot.removeChild(Pack_1_Selection_BTN);
				Pack_1_Selection_BTN.dispose();
			}
			
			if(Pack_3_Selection_BTN != null){
				sceneRoot.removeChild(Pack_3_Selection_Buy_Image);
				Pack_3_Selection_Buy_Image.dispose();
				Pack_3_Selection_BTN.removeEventListener(starling.events.Event.TRIGGERED, Buy_Pack_3);
				sceneRoot.removeChild(Pack_3_Selection_BTN);
				Pack_3_Selection_BTN.dispose();
			}
			
			if(Pack_4_Selection_BTN != null){
				sceneRoot.removeChild(Pack_4_Selection_Buy_Image);
				Pack_4_Selection_Buy_Image.dispose();
				Pack_4_Selection_BTN.removeEventListener(starling.events.Event.TRIGGERED, Buy_Pack_4);
				sceneRoot.removeChild(Pack_4_Selection_BTN);
				Pack_4_Selection_BTN.dispose();
			}
			
			if(Pack_5_Selection_BTN != null){
				sceneRoot.removeChild(Pack_5_Selection_Buy_Image);
				Pack_5_Selection_Buy_Image.dispose();
				Pack_5_Selection_BTN.removeEventListener(starling.events.Event.TRIGGERED, Buy_Pack_5);
				sceneRoot.removeChild(Pack_5_Selection_BTN);
				Pack_5_Selection_BTN.dispose();
			}
			
			IAPManager.manifestUpdated.remove( checkButtons );
			IAPManager.actionCompleted.remove( PurchaseComplete );
			
			Captcha_Holder = null;
			
			//Destroy assets.
			assets.purge();
			assets.dispose();
		}
		
		private function FocusGained(e:flash.events.Event):void{
			trace( "Pack_Selection.FocusGained()" );
			if(!Starling.current.stage3D.visible){
				trace( "Starling not visible, stopping" );
				Starling.current.stop();
			}
		}
		
		private function Buy_Pack_1(e:starling.events.Event):void
		{				
			if(IAPManager.IsPurchased( "ballet_pack" ))
			{
				soundManager.playSound("forwardButton", Costanza.soundVolume);		
				dispatchEventWith( Root.LOAD_PACK, true, "Ballerina");	
			} else if ( !purchasing ){ LaunchSale("ballet_pack"); }
		}
		
		private function Buy_Pack_3(e:starling.events.Event):void
		{				
			if(IAPManager.IsPurchased( "pets_pack" ))
			{
				soundManager.playSound("forwardButton", Costanza.soundVolume);		
				dispatchEventWith(Root.LOAD_PACK, true, "Pets");
			} else if ( !purchasing ){ LaunchSale("pets_pack"); }
		}
		
		private function Buy_Pack_4(e:starling.events.Event):void
		{				
			if(IAPManager.IsPurchased( "princess_pack" ))
			{
				soundManager.playSound("forwardButton", Costanza.soundVolume);		
				dispatchEventWith(Root.LOAD_PACK, true, "Princess");
			} else if ( !purchasing ){ LaunchSale("princess_pack"); }
		}
		
		private function Buy_Pack_5(e:starling.events.Event):void
		{				
			if(IAPManager.IsPurchased( "winter_pack" ))
			{
				soundManager.playSound("forwardButton", Costanza.soundVolume);		
				dispatchEventWith(Root.LOAD_PACK, true, "Winter");
			} else if ( !purchasing ){ LaunchSale( "winter_pack" ); }
		}
		
		private function Buy_Bundle_Deal(e:starling.events.Event):void
		{	
			if(IAPManager.IsPurchased( "bundle_pack" ))
			{
				soundManager.playSound("forwardButton", Costanza.soundVolume);		
				ForceGoBack();
			} else if ( !purchasing ){ LaunchSale( "bundle_pack" ); }
		}
		
		private function LaunchSale( pack:String ):void
		{
			SetFilter();
			Starling.current.stop();
			Starling.current.nativeOverlay.addChild(Captcha_Holder);
			targetPack = pack;
			Captcha.Show( CaptchaReturn, Captcha_Holder );
		}
		
		private function CaptchaReturn( success:Boolean, cancel:Boolean ):void
		{
			Starling.current.nativeOverlay.removeChild( Captcha_Holder );
			Starling.current.start();
			
			if ( success ) { IAPManager.Purchase( targetPack ); }
			else
			{
				this.filter = null;
				purchasing = false;
			}
		}
		
		private function SetFilter():void
		{
			if ( CONFIG::MARKET == "itunes" ) { this.filter = new BlurFilter( 1, 1, .5 ); }
			purchasing = true;
		}
		
		private function PurchaseComplete( success:Boolean ):void { this.filter = null; purchasing = false; trace( "Pack_Selection: Purchase complete" ); }
		
		//Exits.
		private function GoBack(e:starling.events.Event):void{
			if ( !purchasing )
			{
				soundManager.playSound("backwardButton");
				dispatchEventWith(Root.LOAD_MENU, true, "classic");
			}
		}
		
		private function ForceGoBack():void{
			if ( !closing )
			{
				soundManager.playSound("backwardButton");
				dispatchEventWith(Root.LOAD_MENU, true, "classic");
				closing = true;
			}
			
		}
	}
}