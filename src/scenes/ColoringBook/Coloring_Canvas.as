package scenes.ColoringBook{
	
	import com.cupcake.DeviceInfo;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	//This Class is the canvas we draw on.
	//And also hold are possible undos.
	public class Coloring_Canvas extends Sprite{
		
		//Now for some reason to make the getByDef work
		//we need to have them atleast present.
		//So whenever you make a new book be sure to add your available pages.
		//I have no Idea how to prevent this and seems to be an FB issue.
		private var Ballerina_l1:Ballerina_Page_1;
		private var Ballerina_l2:Ballerina_Page_2;
		private var Ballerina_l3:Ballerina_Page_3;
		private var Ballerina_l4:Ballerina_Page_4;
		private var Ballerina_l5:Ballerina_Page_5;
		private var Ballerina_l6:Ballerina_Page_6;
		private var Ballerina_l7:Ballerina_Page_7;
		private var Ballerina_l8:Ballerina_Page_8;
		private var Ballerina_l9:Ballerina_Page_9;
		private var Ballerina_l10:Ballerina_Page_10;
		
		private var BFF_l1:BBF_Page_1;
		private var BFF_l2:BBF_Page_2;
		private var BFF_l3:BBF_Page_3;
		private var BFF_l4:BBF_Page_4;
		private var BFF_l5:BBF_Page_5;
		private var BFF_l6:BBF_Page_6;
		private var BFF_l7:BBF_Page_7;
		private var BFF_l8:BBF_Page_8;
		private var BFF_l9:BBF_Page_9;
		private var BFF_l10:BBF_Page_10;
		
		private var Pets_l1:Pets_Page_1;
		private var Pets_l2:Pets_Page_2;
		private var Pets_l3:Pets_Page_3;
		private var Pets_l4:Pets_Page_4;
		private var Pets_l5:Pets_Page_5;
		private var Pets_l6:Pets_Page_6;
		private var Pets_l7:Pets_Page_7;
		private var Pets_l8:Pets_Page_8;
		private var Pets_l9:Pets_Page_9;
		private var Pets_l10:Pets_Page_10;
		
		private var Princess_l1:Princess_Page_1;
		private var Princess_l2:Princess_Page_2;
		private var Princess_l3:Princess_Page_3;
		private var Princess_l4:Princess_Page_4;
		private var Princess_l5:Princess_Page_5;
		private var Princess_l6:Princess_Page_6;
		private var Princess_l7:Princess_Page_7;
		private var Princess_l8:Princess_Page_8;
		private var Princess_l9:Princess_Page_9;
		private var Princess_l10:Princess_Page_10;
		
		private var Winter_l1:Winter_Page_1;
		private var Winter_l2:Winter_Page_2;
		private var Winter_l3:Winter_Page_3;
		private var Winter_l4:Winter_Page_4;
		private var Winter_l5:Winter_Page_5;
		private var Winter_l6:Winter_Page_6;
		private var Winter_l7:Winter_Page_7;
		private var Winter_l8:Winter_Page_8;
		private var Winter_l9:Winter_Page_9;
		private var Winter_l10:Winter_Page_10;
		
		CONFIG::FULL
		{
			private var Easter_l1:Easter_Page_1;
			private var Easter_l2:Easter_Page_2;
			private var Easter_l3:Easter_Page_3;
			private var Easter_l4:Easter_Page_4;
			private var Easter_l5:Easter_Page_5;
			private var Easter_l6:Easter_Page_6;
			private var Easter_l7:Easter_Page_7;
			private var Easter_l8:Easter_Page_8;
			private var Easter_l9:Easter_Page_9;
			private var Easter_l10:Easter_Page_10;
			
			private var Summer_l1:Summer_Page_1;
			private var Summer_l2:Summer_Page_2;
			private var Summer_l3:Summer_Page_3;
			private var Summer_l4:Summer_Page_4;
			private var Summer_l5:Summer_Page_5;
			private var Summer_l6:Summer_Page_6;
			private var Summer_l7:Summer_Page_7;
			private var Summer_l8:Summer_Page_8;
			private var Summer_l9:Summer_Page_9;
			private var Summer_l10:Summer_Page_10;
		}
		
		//Canvas.
		private var Canvas_Layers:Array;	
		private var Lines:Sprite;
		private var Lines_Image:Bitmap;
		private var Color_Transform:ColorTransform;		
		
		//Properties.
		private var Current_Page:int;
		private var Current_Layer:int;
		
		//Current Layer.
		private var Current_Layer_Data:BitmapData;
		private var m:Matrix;	
		
		//The Current Page.
		private var Page_Class:Class;
		private var Page_Clip:MovieClip;	
		
		//---------------------------------------------------------------------------------
		//Master functions.
		//---------------------------------------------------------------------------------
		public function Coloring_Canvas(){			
			super();			
		}
		public function Load_Page(Prefix:String, Page:int):void{
							
			//Init Canvas.
			if(Canvas_Layers == null){
				Canvas_Layers = new Array();
			}else{Dispose_Canvases();}			
			
			//Set Current Page.
			Current_Page = Page;	
			
			//Set Layer.
			Current_Layer = -1;
			
			//Setup Laer properties.
			var Page_Name:String = (Prefix + "_Page_" + Current_Page.toString());		
			
			//Create the Class for the layer.
			trace( "Looking for class name: '" + Page_Name + "'" );
			try{Page_Class 	= getDefinitionByName(Page_Name) as Class;}
				catch(error:Error){trace("Does Not Exist!"); return;}
			m = new Matrix();
			m.scale(Costanza.SCALE_FACTOR, Costanza.SCALE_FACTOR);
			
			//Get the class created.
			Page_Clip = new Page_Class();
			
			//Go trought the clip.
			for(var X:int = 0; X < Page_Clip.numChildren - 1; X++){
				
				var Layer_Name:String 		= ("p" + ((Canvas_Layers.length) + 1).toString());
				var LayerSprite:Sprite 		= Sprite(Page_Clip.getChildByName(Layer_Name));
				
				if(LayerSprite != null){	
				
					
					//var Layer_Image_Holder:Sprite 	= new Sprite();
					//var Layer_Image:Bitmap 			= new Bitmap();
					//Layer_Image.bitmapData 			= new BitmapData(	(LayerSprite.getChildAt(0).width  * Costanza.SCALE_FACTOR),
																		//(LayerSprite.getChildAt(0).height * Costanza.SCALE_FACTOR),
																		//true, 0x000000);					
					//if(!DeviceInfo.isSlow){
						//Layer_Image.bitmapData.drawWithQuality(LayerSprite.getChildAt(0), m, null, null, null, true, StageQuality.BEST);	
					//}else{
						//Layer_Image.bitmapData.draw(LayerSprite.getChildAt(0), m);	
					//}
					//Layer_Image.x = Math.round(LayerSprite.getChildAt(0).x);
					//Layer_Image.y = Math.round(LayerSprite.getChildAt(0).y);	
					
					//Layer_Image_Holder.addChild(Layer_Image);
					
					//Layer_Image_Holder.x = Math.round(LayerSprite.x) * Costanza.SCALE_FACTOR;
					//Layer_Image_Holder.y = Math.round(LayerSprite.y) * Costanza.SCALE_FACTOR;	
					
					
					//set SWF.
					LayerSprite.getChildAt(0).scaleX 	= Costanza.SCALE_FACTOR;
					LayerSprite.getChildAt(0).scaleY 	= Costanza.SCALE_FACTOR;
					LayerSprite.x 						= Math.round(LayerSprite.x * Costanza.SCALE_FACTOR);
					LayerSprite.y 						= Math.round(LayerSprite.y * Costanza.SCALE_FACTOR);
					
					var Image_Location:Point 		= new Point(Math.round(LayerSprite.getChildAt(0).x), Math.round(LayerSprite.getChildAt(0).y));
					var Image_Holder_Location:Point = new Point(LayerSprite.x, LayerSprite.y);
					var Image_Size:Point 			= new Point(LayerSprite.getChildAt(0).width, LayerSprite.getChildAt(0).height);
					
					var Layer_Prop:Array 			= new Array();					
					Layer_Prop.push(Layer_Name);
					Layer_Prop.push(Image_Location);
					Layer_Prop.push(Image_Holder_Location);
					Layer_Prop.push(Image_Size);
					
					Canvas_Layers.push(Layer_Prop);
					
				}else{	
					
					Lines_Image = new Bitmap();
					Lines_Image.bitmapData = new BitmapData((Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).width * Costanza.SCALE_FACTOR),
															(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).height * Costanza.SCALE_FACTOR),
															true, 0x000000);
					
					if(!DeviceInfo.isSlow){
						Lines_Image.bitmapData.drawWithQuality(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0), m, null, null, null, true, StageQuality.BEST);	
					}else{
						Lines_Image.bitmapData.draw(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0), m);
					}
					
					
					if(!DeviceInfo.isSlow){Lines_Image.smoothing = true;}
					
					Lines_Image.x = Math.round(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).x);
					Lines_Image.y = Math.round(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).y);						
					
					Lines = new Sprite();
					Lines.addChild(Lines_Image);
					Lines.x = Math.round(Sprite(Page_Clip.getChildByName("Lines")).x) * Costanza.SCALE_FACTOR;
					Lines.y = Math.round(Sprite(Page_Clip.getChildByName("Lines")).y) * Costanza.SCALE_FACTOR;						
					
				}				
				
			}					
			
		}
		public function Dispose_Canvases():void{
			
			if(Lines != null){
				Lines.removeChild(Lines_Image);
				Lines = null;
				Lines_Image.bitmapData.dispose();
				Lines_Image = null;
			}
			
			//Destroy patern.
			Page_Clip 	= null;
			Page_Class 	= null;		
			
			//for(var X:int = 0; X < Canvas_Layers.length; X++){
				//Bitmap(Canvas_Layers[X].getChildAt(0)).bitmapData.dispose();
				//Canvas_Layers[X].removeChildAt(0);
				//Canvas_Layers[X] = null;
				//Canvas_Layers.splice(X, 1);
				//X--;
			//}	
			
			Canvas_Layers.length = 0;
			
		}
		
		public function Get_Lines():Sprite{
			return Lines;
		}
		
		
		
		
		public function Select_Layer(Location:Point):int{
			
			//Scan.
			for(var X:int = 0; X < Canvas_Layers.length; X++){				
				if(Layer_Colision(Location, null, X)){		
					
					if(Current_Layer_Data != null){
						Current_Layer_Data.dispose();
						Current_Layer_Data = null;
					}
					
					Current_Layer = X;					
					return X;
				}
			}
			
			//No luck.
			Current_Layer = -1;
			return -1;
			
		}
		public function Get_Layer_Position(Selection:int):Point{
			return Canvas_Layers[Selection][2];
		}
		public function Draw_On_This(Edit_Draw:BitmapData, Color_Selected:uint):void{
			var m:Matrix = new Matrix();
			var CT:ColorTransform = new ColorTransform();
			CT.color = Color_Selected;
			Shape(MovieClip(Page_Clip.getChildByName(Canvas_Layers[Current_Layer][0])).getChildAt(0)).transform.colorTransform = CT;
			m.translate(Canvas_Layers[Current_Layer][2].x, Canvas_Layers[Current_Layer][2].y);
			Edit_Draw.draw(Sprite(Page_Clip.getChildByName(Canvas_Layers[Current_Layer][0])), m);
			m = null;
			CT = null;
		}
		public function Get_Layer_Section(Selection:int, Rec:Rectangle, Position:Point, Edit_Bitmap:Bitmap):void{			
			
			if(Rec == null){
				Rec = new Rectangle(0, 0, Canvas_Layers[Current_Layer][3].x, Canvas_Layers[Current_Layer][3].y);
			}
			
			if(Position == null){
				Position = new Point(0, 0);
			}else{
				Position.x = (Position.x - (Rec.width / 2));
				Position.y = (Position.y - (Rec.height / 2));
			}		
			
			//var Section:Bitmap 	= new Bitmap();
			Edit_Bitmap.bitmapData 	= new BitmapData(Rec.width, Rec.height, true, 0x000000);	
			
			var m:Matrix = new Matrix();
			m.translate(-Position.x, -Position.y);
			Edit_Bitmap.bitmapData.draw(Sprite(Page_Clip.getChildByName(Canvas_Layers[Current_Layer][0])), m);		
			
			if(!DeviceInfo.isSlow){Edit_Bitmap.smoothing = true;}
			m = null;
			//return Section;
			
		}
		public function Get_Layer_Section_Save(Selection:int, Rec:Rectangle = null, Position:Point = null):Bitmap{			
			
			if(Rec == null){
				Rec = new Rectangle(0, 0, Bitmap(Canvas_Layers[Current_Layer].getChildAt(0)).width, Bitmap(Canvas_Layers[Current_Layer].getChildAt(0)).height);
			}
			
			if(Position == null){
				Position = new Point(0, 0);
			}else{
				Position.x = (Position.x - (Rec.width / 2));
				Position.y = (Position.y - (Rec.height / 2));
			}		
			
			var Section:Bitmap 	= new Bitmap();
			Section.bitmapData 	= new BitmapData(Rec.width, Rec.height, true, 0x000000);			
			
			Section.bitmapData.copyPixels(Bitmap(Canvas_Layers[Current_Layer].getChildAt(0)).bitmapData, new Rectangle(Position.x, Position.y, Rec.width, Rec.height), new Point(0, 0));				
			
			if(!DeviceInfo.isSlow){Section.smoothing = true;}
			
			return Section;
			
		}
		public function Layer_Colision(Position:Point, Target:Sprite = null, Location:int = 0):Boolean{
			
			//Set location.			
			var sX:int;
			var sY:int;
			
			if(Target != null){
				
				//Set location.			
				sX =  (Position.x - Canvas_Layers[Location].x);
				sY =  (Position.y - Canvas_Layers[Location].y);
				
				//Check for colision alpha.
				if(Bitmap(Canvas_Layers[Location].getChildAt(0)).bitmapData.getPixel(sX, sY).toString(16) != "0"){			
					//clean.
					return true;
				}else{				
					//Clean.
					return false;				
				}
				
			}
			
			//Set location.			
			sX =  (Position.x - Sprite(Page_Clip.getChildByName(Canvas_Layers[Location][0])).x);
			sY =  (Position.y - Sprite(Page_Clip.getChildByName(Canvas_Layers[Location][0])).y);
			
			
			//Are colision testes color.
			var BitmapColision:BitmapData = new BitmapData(1, 1, true, 0x000000);
			
			//Frame of draw.
			var bRec:Rectangle = new Rectangle(0, 0, 1, 1);
			
			//Translation matrix.
			var bMatrix:Matrix = new Matrix();
			bMatrix.translate(-sX, -sY);
			
			//Draw the pixel.
			BitmapColision.draw(Sprite(Page_Clip.getChildByName(Canvas_Layers[Location][0])), bMatrix, null, null, bRec, false);		
			
			//Check for colision alpha.
			if(BitmapColision.getPixel(0, 0).toString(16) != "0"){			
				//clean.
				BitmapColision.dispose();
				return true;	
			}else{				
				//Clean.
				BitmapColision.dispose();
				return false;				
			}
			
		}
		//---------------------------------------------------------------------------------	
		
	}
}