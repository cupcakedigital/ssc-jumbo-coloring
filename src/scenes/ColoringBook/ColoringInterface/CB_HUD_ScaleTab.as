package scenes.ColoringBook.ColoringInterface{
	
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_HUD_ScaleTab extends Sprite{
		
		private var Active:Boolean;
		private var Definitive:Boolean;
		private var Selected_Scale:Number;
		private var Tool_Sel:int;
		
		private var Scale_1:InterfaceButton;			
		private var Scale_2:InterfaceButton;
		private var Scale_3:InterfaceButton;	
		
		private var List_Of_Scale_Images:Array;
		
		public function CB_HUD_ScaleTab(Core_Assets:External_PNG_Sheet_Manager, Defined:Boolean = false){
			
			if(!Defined){trace("NOT DEFINED----------------------------------");}
			else{trace("YES DEFINED----------------------------------");}
			
			super();
			
			Selected_Scale 			= 1.0;
			Active					= false;
			Definitive				= Defined;
			
			if(!Definitive){
				Scale_1 			= new InterfaceButton(Core_Assets.Get_Image("SH_Size_1_Static"), Core_Assets.Get_Image("SH_Size_1_Glow"));
				Scale_2 			= new InterfaceButton(Core_Assets.Get_Image("SH_Size_2_Static"), Core_Assets.Get_Image("SH_Size_2_Glow"));
				Scale_3 			= new InterfaceButton(Core_Assets.Get_Image("SH_Size_3_Static"), Core_Assets.Get_Image("SH_Size_3_Glow"));	
			}else{			
				
				Scale_1 			= new InterfaceButton(Core_Assets.Get_Image("Holder_Box"), Core_Assets.Get_Image("Holder_Box"));
				Scale_2 			= new InterfaceButton(Core_Assets.Get_Image("Holder_Box"), Core_Assets.Get_Image("Holder_Box"));
				Scale_3 			= new InterfaceButton(Core_Assets.Get_Image("Holder_Box"), Core_Assets.Get_Image("Holder_Box"));				
				
				List_Of_Scale_Images = new Array();
				
				var Brush_Array:Array = new Array();
				
					var Brush_Idle_Array:Array = new Array();
					Brush_Idle_Array.push(Core_Assets.Get_Image("Soft_Circle_Swatch"));
					Brush_Idle_Array.push(Core_Assets.Get_Image("Soft_Circle_Swatch_Med"));
					Brush_Idle_Array.push(Core_Assets.Get_Image("Soft_Circle_Swatch_Small"));
				
					var Brush_Selected_Array:Array = new Array();
					Brush_Selected_Array.push(Core_Assets.Get_Image("Soft_Circle_Swatch_Selected"));
					Brush_Selected_Array.push(Core_Assets.Get_Image("Soft_Circle_Swatch_Med_Selected"));
					Brush_Selected_Array.push(Core_Assets.Get_Image("Soft_Circle_Swatch_Small_Selected"));
					
				Brush_Array.push(Brush_Idle_Array);
				Brush_Array.push(Brush_Selected_Array);
				
				var Chalk_Array:Array = new Array();
				
					var Chalk_Idle_Array:Array = new Array();
					Chalk_Idle_Array.push(Core_Assets.Get_Image("Hard_Spray_Swatch"));
					Chalk_Idle_Array.push(Core_Assets.Get_Image("Hard_Spray_Swatch_Med"));
					Chalk_Idle_Array.push(Core_Assets.Get_Image("Hard_Spray_Swatch_Small"));
					
					var Chalk_Selected_Array:Array = new Array();
					Chalk_Selected_Array.push(Core_Assets.Get_Image("Hard_Spray_Swatch_Selected"));
					Chalk_Selected_Array.push(Core_Assets.Get_Image("Hard_Spray_Swatch_Med_Selected"));
					Chalk_Selected_Array.push(Core_Assets.Get_Image("Hard_Spray_Swatch_Small_Selected"));
				
				Chalk_Array.push(Chalk_Idle_Array);
				Chalk_Array.push(Chalk_Selected_Array);
				
				var Crayon_Array:Array = new Array();
				
					var Crayon_Idle_Array:Array = new Array();
					Crayon_Idle_Array.push(Core_Assets.Get_Image("Fill_Swatch"));
					Crayon_Idle_Array.push(Core_Assets.Get_Image("Fill_Swatch_Med"));
					Crayon_Idle_Array.push(Core_Assets.Get_Image("Fill_Swatch_Small"));
					
					var Crayon_Selected_Array:Array = new Array();
					Crayon_Selected_Array.push(Core_Assets.Get_Image("Fill_Swatch_Selected"));
					Crayon_Selected_Array.push(Core_Assets.Get_Image("Fill_Swatch_Med_Selected"));
					Crayon_Selected_Array.push(Core_Assets.Get_Image("Fill_Swatch_Small_Selected"));
				
				Crayon_Array.push(Crayon_Idle_Array);
				Crayon_Array.push(Crayon_Selected_Array);
				
				var Spray_Array:Array = new Array();
				
					var Spray_Idle_Array:Array = new Array();
					Spray_Idle_Array.push(Core_Assets.Get_Image("Soft_Spray_Swatch"));
					Spray_Idle_Array.push(Core_Assets.Get_Image("Soft_Spray_Swatch_Med"));
					Spray_Idle_Array.push(Core_Assets.Get_Image("Soft_Spray_Swatch_Small"));
					
					var Spray_Selected_Array:Array = new Array();
					Spray_Selected_Array.push(Core_Assets.Get_Image("Soft_Spray_Swatch_Selected"));
					Spray_Selected_Array.push(Core_Assets.Get_Image("Soft_Spray_Swatch_Med_Selected"));
					Spray_Selected_Array.push(Core_Assets.Get_Image("Soft_Spray_Swatch_Small_Selected"));
				
				Spray_Array.push(Spray_Idle_Array);
				Spray_Array.push(Spray_Selected_Array);
				
				
				List_Of_Scale_Images.push(Brush_Array);
				List_Of_Scale_Images.push(Chalk_Array);
				List_Of_Scale_Images.push(Crayon_Array);
				List_Of_Scale_Images.push(Spray_Array);
				
			}
			
			Scale_1.x 				= 0;
			Scale_1.y 				= -(Scale_1.height / 2);
			
			Scale_2.x 				= 0;
			Scale_2.y 				= (-(Scale_1.height / 2) * 3);
			
			Scale_3.x 				= 0;
			Scale_3.y 				= (-(Scale_1.height / 2) * 5);
			
			this.addChild(Scale_1);
			this.addChild(Scale_2);
			this.addChild(Scale_3);
			
		}
		public function Destroy():void{
			
			TweenMax.killTweensOf(this);
			
			this.removeChild(Scale_1);
			this.removeChild(Scale_2);
			this.removeChild(Scale_3);
			
			Scale_1.Destroy();
			Scale_2.Destroy();
			Scale_3.Destroy();
			
			if(List_Of_Scale_Images != null){
				for(var l1:int = 0; l1 < List_Of_Scale_Images.length; l1++){
					for(var l2:int = 0; l2 < List_Of_Scale_Images[l1].length; l2++){
						for(var l3:int = 0; l3 < List_Of_Scale_Images[l1][l2].length; l3++){
							Bitmap(Sprite(List_Of_Scale_Images[l1][l2][l3]).getChildAt(0)).bitmapData.dispose();
							Sprite(List_Of_Scale_Images[l1][l2][l3]).removeChildAt(0);
							List_Of_Scale_Images[l1][l2][l3] = null;							
						}						
					}					
				}
				List_Of_Scale_Images.lenght = 0;
			}
			
		}	
		public function Activate(BrushType:String = ""):void{		
			
			if(Definitive){	
				
				switch(BrushType){
					case "Brush":{Tool_Sel 	= 0;}break;				
					case "Crayon":{Tool_Sel = 2;}break;
					case "Spray":{Tool_Sel 	= 3;}break;
					case "Chalk":{Tool_Sel 	= 1;}break;
					case "Eraser":{Tool_Sel = 2;}break;
				}
				
				if(Scale_1.numChildren > 2){Scale_1.removeChildAt((Scale_1.numChildren - 1));}
				if(Scale_2.numChildren > 2){Scale_2.removeChildAt((Scale_2.numChildren - 1));}
				if(Scale_3.numChildren > 2){Scale_3.removeChildAt((Scale_3.numChildren - 1));}
				
				Position_Center(List_Of_Scale_Images[Tool_Sel][0][2]);
				Position_Center(List_Of_Scale_Images[Tool_Sel][0][1]);
				Position_Center(List_Of_Scale_Images[Tool_Sel][0][0]);
				
				Scale_1.addChild(List_Of_Scale_Images[Tool_Sel][0][2]);
				Scale_2.addChild(List_Of_Scale_Images[Tool_Sel][0][1]);
				Scale_3.addChild(List_Of_Scale_Images[Tool_Sel][0][0]);
			}			
			
			
			switch(Selected_Scale){
				case 0.5:{
					Scale_1.Force_Activate();
					if(Definitive){
						Scale_1.removeChildAt((Scale_1.numChildren - 1));
						Position_Center(List_Of_Scale_Images[Tool_Sel][1][2]);
						Scale_1.addChild(List_Of_Scale_Images[Tool_Sel][1][2]);
					}					
				}break;
				case 1.0:{
					Scale_2.Force_Activate();
					if(Definitive){
						Scale_2.removeChildAt((Scale_2.numChildren - 1));
						Position_Center(List_Of_Scale_Images[Tool_Sel][1][1]);
						Scale_2.addChild(List_Of_Scale_Images[Tool_Sel][1][1]);
					}	
				}break;
				case 1.5:{
					Scale_3.Force_Activate();
					if(Definitive){
						Scale_3.removeChildAt((Scale_3.numChildren - 1));
						Position_Center(List_Of_Scale_Images[Tool_Sel][1][0]);
						Scale_3.addChild(List_Of_Scale_Images[Tool_Sel][1][0]);
					}	
				}break;
			}
			
			Active = true;
			Show();
			
		}
		public function Deactivate():void{
			Scale_1.Reset();
			Scale_2.Reset();
			Scale_3.Reset();
			Active = false;
			Hide();
		}
		public function checkActivated():Boolean{return Active;}
		private function Position_Center(sPrite:Sprite):void{
			sPrite.x = -(sPrite.width / 2);
			sPrite.y = -(sPrite.height / 2);
		}
		
		//Inputs.
		public function Hover(MousePosition:Point):String{				
			
			Scale_1.Reset();
			Scale_2.Reset();
			Scale_3.Reset();
			
			if(Scale_1.Down(MousePosition)){
				Selected_Scale = 0.5;
				Scale_2.Reset();
				Scale_3.Reset();
				return "Scale_1";
			}
			if(Scale_2.Down(MousePosition)){
				Selected_Scale = 1.0;
				Scale_1.Reset();
				Scale_3.Reset();
				return "Scale_2";
			}
			if(Scale_3.Down(MousePosition)){
				Selected_Scale = 1.5;
				Scale_2.Reset();
				Scale_1.Reset();
				return "Scale_3";
			}
			
			return "Nothing";
		}
		
		//Animate.
		public function Show():void{
			TweenMax.killTweensOf(this);
			TweenMax.to(this, 0.2, {alpha:1});
		}
		public function Hide():void{
			TweenMax.killTweensOf(this);
			TweenMax.to(this, 0.2, {alpha:0});
		}
		
		//Return Values.
		public function getScale():Number{return Selected_Scale;}
		
	}
}