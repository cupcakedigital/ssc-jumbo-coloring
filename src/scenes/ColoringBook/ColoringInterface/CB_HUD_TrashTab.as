package scenes.ColoringBook.ColoringInterface{
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_HUD_TrashTab extends Sprite{
		
		//Buttons.
		private var TrashCan:InterfaceButton;	
		
		public function CB_HUD_TrashTab(Core_Assets:External_PNG_Sheet_Manager){
			
			super();			
			
			TrashCan = new InterfaceButton(Core_Assets.Get_Image("CB_Sticker_Garbage_Static"), Core_Assets.Get_Image("CB_Sticker_Garbage_Glow"));
			
			this.addChild(TrashCan);
			
		}
		public function Destroy():void{
			
			this.removeChild(TrashCan);
			TrashCan.Destroy();
			
		}
		
		//Inputs.
		public function Down(MousePosition:Point):String{			
			
			if(TrashCan.Down(MousePosition)){return "TrashCan";}			
			return "Nothing";
			
		}
		public function Up(MousePosition:Point):String{
			
			TrashCan.Reset();
			
			if(TrashCan.Up(MousePosition)){return "TrashCan";}			
			return "Nothing";
			
		}		
		
	}
}