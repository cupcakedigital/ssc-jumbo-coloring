package scenes.ColoringBook.ColoringInterface{
	
	import com.cupcake.IAPManager;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import scenes.ColoringBook.Coloring_Book_Scene;
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_Upsell_Selection extends Sprite{
		
		private var isActive:Boolean;
		
		private var bg:flash.display.Sprite;
		
		private var Return_Button:InterfaceButton;		
		private var UpSell_Deal_Selection_BTN:InterfaceButton;
		
		private var Pack_1_Selection_BTN:InterfaceButton;
		private var Pack_3_Selection_BTN:InterfaceButton;
		private var Pack_4_Selection_BTN:InterfaceButton;
		private var Pack_5_Selection_BTN:InterfaceButton;
		
		private var Pack_1_Selection_Buy_Image:flash.display.Sprite;
		private var Pack_3_Selection_Buy_Image:flash.display.Sprite;
		private var Pack_4_Selection_Buy_Image:flash.display.Sprite;
		private var Pack_5_Selection_Buy_Image:flash.display.Sprite;
		
		private var Pack_1_Selection_Play_Image:flash.display.Sprite;
		private var Pack_3_Selection_Play_Image:flash.display.Sprite;
		private var Pack_4_Selection_Play_Image:flash.display.Sprite;
		private var Pack_5_Selection_Play_Image:flash.display.Sprite;	
		
		private var pack1Price:TextField;
		private var pack3Price:TextField;
		private var pack4Price:TextField;
		private var pack5Price:TextField;
		private var bundlePrice:TextField;
		
		private var CB:Coloring_Book_Scene;
		
		private var priceFormat:TextFormat = new TextFormat( "_sans", 24 * Costanza.SCALE_FACTOR, 0xFFFFFF, true, null, null, null, null, TextFormatAlign.CENTER );
		
		public static const TEXT_HEIGHT:int		= 30 * Costanza.SCALE_FACTOR;
		public static const TEXT_WIDTH:int		= 100 * Costanza.SCALE_FACTOR;
		
		private var BFill:BlurFilter = new BlurFilter(4, 4, 2);
		
		private var purchasing:Boolean;
		
		public function CB_Upsell_Selection(Core_Assets:External_PNG_Sheet_Manager, theCB:Coloring_Book_Scene){
			
			super();
			
			CB = theCB;
			
			purchasing 								= false;
			isActive								= false;
			
			//Background.
			bg = Core_Assets.Get_Image("UpSell_Background");
			this.addChild(bg);		
			
			//The Deal.			
			UpSell_Deal_Selection_BTN 				= new InterfaceButton(Core_Assets.Get_Image("UpSell_Deal_Selection"));
			UpSell_Deal_Selection_BTN.x 			= ((1366 * Costanza.SCALE_FACTOR) / 2);
			UpSell_Deal_Selection_BTN.y 			= ((5 * Costanza.SCALE_FACTOR) + (UpSell_Deal_Selection_BTN.height / 2));
			this.addChild(UpSell_Deal_Selection_BTN);
			
			bundlePrice							= new TextField;
			bundlePrice.width					= TEXT_WIDTH;
			bundlePrice.height					= TEXT_HEIGHT;
			bundlePrice.text 					= IAPManager.GetProduct( "bundle_pack" ).price;
			trace( IAPManager.GetProduct( "bundle_pack" ).price );
			bundlePrice.setTextFormat( priceFormat );
			bundlePrice.x						= (UpSell_Deal_Selection_BTN.x - (30 * Costanza.SCALE_FACTOR));
			bundlePrice.y						= (UpSell_Deal_Selection_BTN.y + (95 * Costanza.SCALE_FACTOR));
			bundlePrice.visible					= true;
			trace( "Bundle price coords: " + bundlePrice.x + ", " + bundlePrice.y );
			this.addChild( bundlePrice );
			
			Pack_1_Selection_BTN					= new InterfaceButton(Core_Assets.Get_Image("UpSell_Ballerina_Selection"));
			Pack_1_Selection_BTN.x 					= ((171 * Costanza.SCALE_FACTOR) + (Pack_1_Selection_BTN.width / 2));
			Pack_1_Selection_BTN.y 					= ((350 * Costanza.SCALE_FACTOR) + (Pack_1_Selection_BTN.height / 2));
			this.addChild(Pack_1_Selection_BTN);
			
			Pack_1_Selection_Buy_Image 			=  Core_Assets.Get_Image("UpSell_Purchase_Icon");
			Pack_1_Selection_Buy_Image.name 	= "buy";
			Pack_1_Selection_Buy_Image.x 		= (Pack_1_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_1_Selection_BTN.width / 2);
			Pack_1_Selection_Buy_Image.y 		= (Pack_1_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_1_Selection_BTN.height / 2);
			
			Pack_1_Selection_Play_Image 		=  Core_Assets.Get_Image("UpSell_Play_Icon");
			Pack_1_Selection_Play_Image.name 	= "play";
			Pack_1_Selection_Play_Image.x 		= (Pack_1_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_1_Selection_BTN.width / 2);
			Pack_1_Selection_Play_Image.y 		= (Pack_1_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_1_Selection_BTN.height / 2);
			
			if(!IAPManager.IsPurchased( "ballet_pack" )){				
				this.addChild(Pack_1_Selection_Buy_Image);
			}else{				
				this.addChild(Pack_1_Selection_Play_Image);
			}
			
			pack1Price							= new TextField;
			pack1Price.width					= TEXT_WIDTH;
			pack1Price.height					= TEXT_HEIGHT;
			pack1Price.text 					= IAPManager.GetProduct( "ballet_pack" ).price;
			pack1Price.setTextFormat( priceFormat );
			pack1Price.x						= (Pack_1_Selection_Buy_Image.x + (90 * Costanza.SCALE_FACTOR));
			pack1Price.y						= (Pack_1_Selection_Buy_Image.y + (40 * Costanza.SCALE_FACTOR));
			trace( "Pack 1 price coords: " + pack1Price.x + ", " + pack1Price.y );
			pack1Price.visible					= !IAPManager.IsPurchased( "ballet_pack" );
			this.addChild( pack1Price );
			
			Pack_3_Selection_BTN					= new InterfaceButton(Core_Assets.Get_Image("UpSell_Pets_Selection"));
			Pack_3_Selection_BTN.x 					= ((171 * Costanza.SCALE_FACTOR) + (1 * (Pack_3_Selection_BTN.width - (15 * Costanza.SCALE_FACTOR)))) + (Pack_3_Selection_BTN.width / 2) + (1 * (17 * Costanza.SCALE_FACTOR));
			Pack_3_Selection_BTN.y 					= ((350 * Costanza.SCALE_FACTOR) + (Pack_3_Selection_BTN.height / 2));
			this.addChild(Pack_3_Selection_BTN);
			
			Pack_3_Selection_Buy_Image 			=  Core_Assets.Get_Image("UpSell_Purchase_Icon");
			Pack_3_Selection_Buy_Image.name 	= "buy";
			Pack_3_Selection_Buy_Image.x 		= (Pack_3_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_3_Selection_BTN.width / 2);
			Pack_3_Selection_Buy_Image.y 		= (Pack_3_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_3_Selection_BTN.height / 2);
			
			Pack_3_Selection_Play_Image 		=  Core_Assets.Get_Image("UpSell_Play_Icon");
			Pack_3_Selection_Play_Image.name 	= "play";
			Pack_3_Selection_Play_Image.x 		= (Pack_3_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_3_Selection_BTN.width / 2);
			Pack_3_Selection_Play_Image.y 		= (Pack_3_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_3_Selection_BTN.height / 2);
			
			if(!IAPManager.IsPurchased( "pets_pack" )){			
				this.addChild(Pack_3_Selection_Buy_Image);	
			}else{				
				this.addChild(Pack_3_Selection_Play_Image);	
			}	
			
			pack3Price							= new TextField;
			pack3Price.width					= TEXT_WIDTH;
			pack3Price.height					= TEXT_HEIGHT;
			pack3Price.text 					= IAPManager.GetProduct( "pets_pack" ).price;
			pack3Price.setTextFormat( priceFormat );
			pack3Price.x						= (Pack_3_Selection_Buy_Image.x + (90 * Costanza.SCALE_FACTOR));
			pack3Price.y						= (Pack_3_Selection_Buy_Image.y + (40 * Costanza.SCALE_FACTOR));
			pack3Price.visible					= !IAPManager.IsPurchased( "pets_pack" );
			this.addChild( pack3Price );
			
			Pack_4_Selection_BTN					= new InterfaceButton(Core_Assets.Get_Image("UpSell_Princess_Selection"));
			Pack_4_Selection_BTN.x 					= ((171 * Costanza.SCALE_FACTOR) + (2 * (Pack_4_Selection_BTN.width - (15 * Costanza.SCALE_FACTOR)))) + (Pack_4_Selection_BTN.width / 2) + (2 * (17 * Costanza.SCALE_FACTOR));
			Pack_4_Selection_BTN.y 					= ((350 * Costanza.SCALE_FACTOR) + (Pack_4_Selection_BTN.height / 2));
			this.addChild(Pack_4_Selection_BTN);
			
			Pack_4_Selection_Buy_Image 			=  Core_Assets.Get_Image("UpSell_Purchase_Icon");
			Pack_4_Selection_Buy_Image.name 	= "buy";
			Pack_4_Selection_Buy_Image.x 		= (Pack_4_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_4_Selection_BTN.width / 2);
			Pack_4_Selection_Buy_Image.y 		= (Pack_4_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_4_Selection_BTN.height / 2);
			
			Pack_4_Selection_Play_Image 		=  Core_Assets.Get_Image("UpSell_Play_Icon");
			Pack_4_Selection_Play_Image.name 	= "play";
			Pack_4_Selection_Play_Image.x 		= (Pack_4_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_4_Selection_BTN.width / 2);
			Pack_4_Selection_Play_Image.y 		= (Pack_4_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_4_Selection_BTN.height / 2);
			
			if(!IAPManager.IsPurchased( "princess_pack" )){			
				this.addChild(Pack_4_Selection_Buy_Image);
			}else{				
				this.addChild(Pack_4_Selection_Play_Image);
			}	
			
			pack4Price							= new TextField;
			pack4Price.width					= TEXT_WIDTH;
			pack4Price.height					= TEXT_HEIGHT;
			pack4Price.text 					= IAPManager.GetProduct( "princess_pack" ).price;
			pack4Price.setTextFormat( priceFormat );
			pack4Price.x						= (Pack_4_Selection_Buy_Image.x + (90 * Costanza.SCALE_FACTOR));
			pack4Price.y						= (Pack_4_Selection_Buy_Image.y + (40 * Costanza.SCALE_FACTOR));
			pack4Price.visible					= !IAPManager.IsPurchased( "princess_pack" );
			this.addChild( pack4Price );
			
			Pack_5_Selection_BTN					= new InterfaceButton(Core_Assets.Get_Image("UpSell_Winter_Selection"));
			Pack_5_Selection_BTN.x 					= ((171 * Costanza.SCALE_FACTOR) + (3 * (Pack_5_Selection_BTN.width - (15 * Costanza.SCALE_FACTOR)))) + (Pack_5_Selection_BTN.width / 2) + (3 * (17 * Costanza.SCALE_FACTOR));
			Pack_5_Selection_BTN.y 					= ((350 * Costanza.SCALE_FACTOR) + (Pack_5_Selection_BTN.height / 2));		
			this.addChild(Pack_5_Selection_BTN);
			
			Pack_5_Selection_Buy_Image 			=  Core_Assets.Get_Image("UpSell_Purchase_Icon");
			Pack_5_Selection_Buy_Image.name 	= "buy";
			Pack_5_Selection_Buy_Image.x 		= (Pack_5_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_5_Selection_BTN.width / 2);
			Pack_5_Selection_Buy_Image.y 		= (Pack_5_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_5_Selection_BTN.height / 2);
			
			Pack_5_Selection_Play_Image 		=  Core_Assets.Get_Image("UpSell_Play_Icon");	
			Pack_5_Selection_Play_Image.name 	= "play";
			Pack_5_Selection_Play_Image.x 		= (Pack_5_Selection_BTN.x + (15 * Costanza.SCALE_FACTOR)) - (Pack_5_Selection_BTN.width / 2);
			Pack_5_Selection_Play_Image.y 		= (Pack_5_Selection_BTN.y + (325 * Costanza.SCALE_FACTOR)) - (Pack_5_Selection_BTN.height / 2);
			
			if(!IAPManager.IsPurchased( "winter_pack" )){			
				this.addChild(Pack_5_Selection_Buy_Image);
			}else{			
				this.addChild(Pack_5_Selection_Play_Image);
			}	
			
			pack5Price							= new TextField;
			pack5Price.width					= TEXT_WIDTH;
			pack5Price.height					= TEXT_HEIGHT;
			pack5Price.text 					= IAPManager.GetProduct( "winter_pack" ).price;
			pack5Price.setTextFormat( priceFormat );
			pack5Price.x						= (Pack_5_Selection_Buy_Image.x + (90 * Costanza.SCALE_FACTOR));
			pack5Price.y						= (Pack_5_Selection_Buy_Image.y + (40 * Costanza.SCALE_FACTOR));
			pack5Price.visible					= !IAPManager.IsPurchased( "winter_pack" );
			this.addChild( pack5Price );
			
			//Return.
			Return_Button							= new InterfaceButton(Core_Assets.Get_Image("xtc"));	
			Return_Button.scaleX 					= .7;
			Return_Button.scaleY 					= .7;	
			Return_Button.x 						= (((1024 + 171) * Costanza.SCALE_FACTOR) - (Return_Button.width) + (40 * Costanza.SCALE_FACTOR));
			Return_Button.y 						= (Return_Button.height / 2);
			
					
			this.addChild(Return_Button);		
			
			IAPManager.manifestUpdated.add( Update_Buttons );
			IAPManager.actionCompleted.add( PurchaseComplete );
			
		}
		public function Destroy():void{
			
			this.removeChild(bg);
			this.removeChild(Return_Button);
			this.removeChild(UpSell_Deal_Selection_BTN);
			this.removeChild(Pack_1_Selection_BTN);
			this.removeChild(Pack_3_Selection_BTN);
			this.removeChild(Pack_4_Selection_BTN);
			this.removeChild(Pack_5_Selection_BTN);
			if(this.contains(Pack_1_Selection_Buy_Image)){this.removeChild(Pack_1_Selection_Buy_Image);}
			if(this.contains(Pack_3_Selection_Buy_Image)){this.removeChild(Pack_3_Selection_Buy_Image);}
			if(this.contains(Pack_4_Selection_Buy_Image)){this.removeChild(Pack_4_Selection_Buy_Image);}
			if(this.contains(Pack_5_Selection_Buy_Image)){this.removeChild(Pack_5_Selection_Buy_Image);}
			if(this.contains(Pack_1_Selection_Play_Image)){this.removeChild(Pack_1_Selection_Play_Image);}
			if(this.contains(Pack_3_Selection_Play_Image)){this.removeChild(Pack_3_Selection_Play_Image);}
			if(this.contains(Pack_4_Selection_Play_Image)){this.removeChild(Pack_4_Selection_Play_Image);}
			if(this.contains(Pack_5_Selection_Play_Image)){this.removeChild(Pack_5_Selection_Play_Image);}
			
			Return_Button.Destroy();
			UpSell_Deal_Selection_BTN.Destroy();
			Pack_1_Selection_BTN.Destroy();
			Pack_3_Selection_BTN.Destroy();
			Pack_4_Selection_BTN.Destroy();
			Pack_5_Selection_BTN.Destroy();
			
			Dispose_Sprite(bg); bg = null;
			
			Dispose_Sprite(Pack_1_Selection_Buy_Image); Pack_1_Selection_Buy_Image = null;
			Dispose_Sprite(Pack_3_Selection_Buy_Image); Pack_3_Selection_Buy_Image = null;
			Dispose_Sprite(Pack_4_Selection_Buy_Image); Pack_4_Selection_Buy_Image = null;
			Dispose_Sprite(Pack_5_Selection_Buy_Image); Pack_5_Selection_Buy_Image = null;
			
			Dispose_Sprite(Pack_1_Selection_Play_Image); Pack_1_Selection_Play_Image = null;
			Dispose_Sprite(Pack_3_Selection_Play_Image); Pack_3_Selection_Play_Image = null;
			Dispose_Sprite(Pack_4_Selection_Play_Image); Pack_4_Selection_Play_Image = null;
			Dispose_Sprite(Pack_5_Selection_Play_Image); Pack_5_Selection_Play_Image = null;
			
			IAPManager.manifestUpdated.remove( Update_Buttons );
			IAPManager.actionCompleted.remove( PurchaseComplete );
		}
		private function Dispose_Sprite(aS:flash.display.Sprite):void{
			
			Bitmap(aS.getChildAt(0)).bitmapData.dispose();
			aS.removeChildAt(0);
		}
		
		private function Update_Buttons():String{
			
			if(IAPManager.IsPurchased( "bundle_pack" )){
				CB.CloseUpsell();
				return "Bundle";
			}
			else{
				if ((IAPManager.IsPurchased( "winter_pack" )) && this.contains(Pack_5_Selection_Buy_Image)){
					this.removeChild(Pack_5_Selection_Buy_Image);
					this.addChild(Pack_5_Selection_Play_Image);
					pack5Price.visible = false;
				}
				if ((IAPManager.IsPurchased( "princess_pack" )) && this.contains(Pack_4_Selection_Buy_Image)){
					this.removeChild(Pack_4_Selection_Buy_Image);
					this.addChild(Pack_4_Selection_Play_Image);
					pack4Price.visible = false;
				}
				if ((IAPManager.IsPurchased( "pets_pack" )) && this.contains(Pack_3_Selection_Buy_Image)){
					this.removeChild(Pack_3_Selection_Buy_Image);
					this.addChild(Pack_3_Selection_Play_Image);
					pack3Price.visible = false;
				}
				if ((IAPManager.IsPurchased( "ballet_pack" )) && this.contains(Pack_1_Selection_Buy_Image)){
					this.removeChild(Pack_1_Selection_Buy_Image);
					this.addChild(Pack_1_Selection_Play_Image);
					pack1Price.visible = false;
				}
			}
			
			return "Nothing";
			
		}
		
		public function Down(MousePosition:Point):String{			
			
			trace("DOMW");
			UpSell_Deal_Selection_BTN.Reset();			
			Pack_1_Selection_BTN.Reset();
			Pack_3_Selection_BTN.Reset();
			Pack_4_Selection_BTN.Reset();
			Pack_5_Selection_BTN.Reset();
			Return_Button.Reset();
			
			if(Return_Button.Down(MousePosition)){
				if(!purchasing){
					return "CloseMe";
				}
			}
			if(UpSell_Deal_Selection_BTN.Down(MousePosition)){
				if(IAPManager.IsPurchased( "bundle_pack" ) && !purchasing){
					return "Bundle";
				}else if(!purchasing){
					SetFilter();
					IAPManager.Purchase("bundle_pack");
					//return Update_Buttons();
				}
			}
			if(Pack_1_Selection_BTN.Down(MousePosition)){
				if(IAPManager.IsPurchased( "ballet_pack" ) && !purchasing){	
					return "Load_Ballerina";
				}else if(!purchasing){
					SetFilter();
					IAPManager.Purchase("ballet_pack");
					//return Update_Buttons();
				}
			}
			if(Pack_3_Selection_BTN.Down(MousePosition)){
				if(IAPManager.IsPurchased( "pets_pack" ) && !purchasing){	
					return "Load_Pets";
				}else if(!purchasing){
					SetFilter();
					IAPManager.Purchase("pets_pack");
					//return Update_Buttons();
				}
			}
			if(Pack_4_Selection_BTN.Down(MousePosition)){
				if(IAPManager.IsPurchased( "princess_pack" ) && !purchasing){	
					return "Load_Princess";
				}else if(!purchasing){
					SetFilter();
					IAPManager.Purchase("princess_pack");
					//return Update_Buttons();
				}
			}
			if(Pack_5_Selection_BTN.Down(MousePosition)){
				trace("Pack 5");
				if(IAPManager.IsPurchased( "winter_pack" ) && !purchasing){
					trace("Git 5");
					return "Load_Winter";
				}else if(!purchasing){
					trace("Buy Pack 5");
					SetFilter();
					IAPManager.Purchase("winter_pack");
					//return Update_Buttons();
				}
			}
			
			return "Nothing";
		}	
		
		private function SetFilter():void
		{
			if ( CONFIG::MARKET == "itunes" ) { this.filters = [ BFill ]; }
			purchasing = true;
		}
		
		private function PurchaseComplete( success:Boolean ):void { this.filters = []; purchasing = false; trace( "Pack_Selection: Purchase complete" ); }
		
		public function Activate():void{isActive = true;}
		public function Deactivate():void{isActive = false;}
		public function Get_Active():Boolean{return isActive;}
		
	}
}