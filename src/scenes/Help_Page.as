package scenes{
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	public class Help_Page extends Sprite{		
		
		//Background image.
		private var Background:Image;
		private var Active:Boolean;
		
		public function Help_Page(assets:AssetManager){
			
			super();
			
			//Setup Background.
			Background = new Image(assets.getTexture("Help_Page"));
			this.addChild(Background);	
			
			Active = false;
			
		}
		
		public function Activate():void{Active = true;}
		public function Deactivate():void{Active = false;}
		public function isActive():Boolean{return Active;}
		
		public function Dispose():void{
			//Delete Background.
			this.removeChild(Background);
			Background.dispose();
		}
		
	}
}