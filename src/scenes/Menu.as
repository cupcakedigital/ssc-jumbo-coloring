package scenes{   
	
    import com.cupcake.App;
    import com.cupcake.DeviceInfo;
    import com.cupcake.IAPManager;
    import com.cupcake.MemoryTracker;
    import com.greensock.TweenMax;
    import com.greensock.easing.Bounce;
    import com.greensock.events.LoaderEvent;
    import com.greensock.loading.ImageLoader;
    import com.greensock.loading.LoaderMax;
    
    import flash.display.Bitmap;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.filesystem.File;
    import flash.geom.Rectangle;
    import flash.system.Capabilities;
    import flash.system.System;
    
    import pl.mateuszmackowiak.nativeANE.dialogs.NativeAlertDialog;
    
    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.extensions.PDParticleSystem;
    import starling.extensions.SoundManager;
    import starling.filters.BlurFilter;
    import starling.textures.Texture;
    import starling.utils.AssetManager;
    
    import ui.Settings;
    
    import utils.ButtonAlpha;
    import utils.ForParentsScreen;
    import utils.MoreApps;

    public class Menu extends starling.display.Sprite{
		
		//Core.
		private 	   var soundManager:SoundManager;
		private static var assets:AssetManager;
		private 	   var appDir:File;
		private        var assetsLoaded:Boolean;				

		//Seting Pop up Box.
		private var Settings_Box:Settings;
		
		//Background image.
		private var Background:Image;	
		
		//More Apps Holder.
		private var MoreApps_Holder:flash.display.Sprite;
		
		//Parents Help Holder.
		private var ParentsHelp_Holder:flash.display.Sprite;	
		
		private var scroller:starling.display.Sprite;
		
		//Packs.
		private var Parents_BTN:ButtonAlpha;
		private var MoreApps_BTN:ButtonAlpha;
		private var Options_BTN:ButtonAlpha;
		
		private var Selection_ID:String;
		private var scrolling:Boolean;
		
		private var restoreBtn:Button;
		private var restoring:Boolean = false;
		
		private var lastX:int;
		private var firstX:int;
		
		private var appBtnSprite:flash.display.Sprite;
		
		private var appImageDisplay:Button;
		
		private var loader:LoaderMax;
		
		private var ldr:ImageLoader;
		
		private var appsSparkle:PDParticleSystem;		
		
		private var HelpPage:Help_Page;
		private var CreditsPage:Credits_Page;
		
		private var blurFilter:BlurFilter;
		
		private var packButtons:Vector.<ButtonAlpha>	= new Vector.<ButtonAlpha>;
		private var packLocks:Vector.<ButtonAlpha>		= new Vector.<ButtonAlpha>;
		
		
		
		//--------------------------------------------------------------------------------
		//Core.
		//--------------------------------------------------------------------------------
        public function Menu(Borg:String = ""){		
			MemoryTracker.track( this, "Main Menu" );
			
			assetsLoaded 			= false;
			appDir 					= File.applicationDirectory;			
			
			assets 					= new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps 		= Costanza.mipmapsEnabled;
			assets.verbose 			= Capabilities.isDebugger;
			
			assets.enqueue(
				appDir.resolvePath("textures/global/x" + Costanza.SCALE_FACTOR.toString() + "/"),
				appDir.resolvePath("textures/menu/" + Costanza.RELEASE + "/x" + Costanza.SCALE_FACTOR.toString() + "/"),
				appDir.resolvePath("textures/menu/x" + Costanza.SCALE_FACTOR.toString() + "/")			
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void{				
				if(ratio == 1){
					Starling.juggler.delayCall(function():void{						
						assetsLoaded = true;							
						Build_Scene();
					}, 0.15);
					System.pauseForGCIfCollectionImminent(0);
					System.gc();	
				};
			});
		}   	
		
		private function Build_Scene():void
		{	
			IAPManager.actionCompleted.add( RestoreComplete );
			IAPManager.manifestUpdated.add( CheckPacks );
			
			Selection_ID 	= "";
			scrolling 		= false;
			lastX			= 0;
			firstX			= 0;
			
			//Loadup Moreapps.
			MoreApps.Setup(MoreApps_Loaded, "en");

			//Get Sounds Manager.
			soundManager = SoundManager.getInstance();	
			soundManager.playSound("Menu", Costanza.musicVolume,999);
			
			HelpPage 		= new Help_Page(assets);
			CreditsPage 	= new Credits_Page(assets);
			
			//Setup Background.
			Background = new Image(assets.getTexture("Menu_Background"));
			this.addChild(Background);		
			
			//button for refreshing purchases
			CONFIG::FREE
			{
				restoreBtn = new Button(assets.getTexture("restore_up"),"",assets.getTexture("restore_down"));
				restoreBtn.x = 195;
				restoreBtn.y = 680;
				restoreBtn.addEventListener(starling.events.Event.TRIGGERED,restoreTapped);
				if ( CONFIG::MARKET != "itunes" || CONFIG::FULL )
				{
					restoreBtn.visible = false;
					restoreBtn.enabled = false;
				}
				this.addChild(restoreBtn);
			}
			
			
			//Setup More Apps Holder.
			MoreApps_Holder 			= new flash.display.Sprite();
			MoreApps_Holder.x 			= (171 + Costanza.STAGE_OFFSET);
			
			//Setup Parents Help Holder.
			ParentsHelp_Holder 			= new flash.display.Sprite();
			ParentsHelp_Holder.x 		= (171 + Costanza.STAGE_OFFSET);
			
			scroller = new starling.display.Sprite();
			this.addChild(scroller);
			
			Parents_BTN				= new ButtonAlpha(assets.getTexture("Menu_ForParents_Icon"));
			Parents_BTN.addEventListener(starling.events.Event.TRIGGERED, ParentsHelp_Selected);
			Parents_BTN.x 			= 1000 - (Parents_BTN.width  / 2);
			Parents_BTN.y 			= 720 - (Parents_BTN.height / 2);
			Parents_BTN.touchable 	= true;		
			Parents_BTN.scaleWhenDown = 1.0;
			Parents_BTN.enabled		= false;
			Parents_BTN.visible		= false;
			this.addChild(Parents_BTN);
			
			MoreApps_BTN			= new ButtonAlpha(assets.getTexture("Menu_MoreApp_Icon"));
			MoreApps_BTN.addEventListener(starling.events.Event.TRIGGERED, MoreApps_Selected);
			MoreApps_BTN.x 			= 890 - (Parents_BTN.width  / 2);
			MoreApps_BTN.y 			= 720 - (Parents_BTN.height / 2);
			MoreApps_BTN.touchable 	= true;		
			MoreApps_BTN.scaleWhenDown = 1.0;
			MoreApps_BTN.enabled	= false;
			MoreApps_BTN.visible	= false;
			this.addChild(MoreApps_BTN);
			
			Options_BTN			= new ButtonAlpha(assets.getTexture("SSC_Options"));
			Options_BTN.addEventListener(starling.events.Event.TRIGGERED, Options_Selected);
			Options_BTN.x 			= 1110 - (Options_BTN.width  / 2);
			Options_BTN.y 			= 720 - (Options_BTN.height / 2);
			Options_BTN.touchable 	= true;		
			Options_BTN.scaleWhenDown = 1.0;
			this.addChild(Options_BTN);
			
			var btn:ButtonAlpha;
			var lock:ButtonAlpha;
			for ( var t:int = 0 ; t < Costanza.packNames.length ; t++ )
			{
				soundManager.addSound( Costanza.packNames[ t ] + "_Selection", (Root.assets.getSound("main screen - " + Costanza.packSounds[ t ] + " button")));
				
				trace( "Now loading texture for " + Costanza.packLabels[ t ] );
				btn						= new ButtonAlpha(assets.getTexture("Menu_" + Costanza.packLabels[ t ] + "_Selection"),"",
														  assets.getTexture("Menu_" + Costanza.packLabels[ t ] + "_Selection_Over"));
				btn.addEventListener(starling.events.Event.TRIGGERED, Pack_Selected);
				btn.name				= Costanza.packNames[ t ];
				btn.x 					= (180 + (t * 260));
				btn.y 					= ((768 / 2) - (btn.height / 2)) + 70;
				btn.touchable 			= true;		
				btn.scaleWhenDown 		= 1.0;
				scroller.addChild(btn);
				packButtons.push( btn );
				
				CONFIG::FREE
				{
					lock				= new ButtonAlpha(assets.getTexture("LOCK"));
					lock.name			= Costanza.packNames[ t ];
					lock.x				= ((btn.x + (btn.width)) - (lock.width)) - 40;
					lock.y				= ((btn.y + (btn.height)) - (lock.height)) - 70;
					lock.visible 		= ( t > 0 && !IAPManager.IsPurchased( "bundle_pack" ) && !IAPManager.IsPurchased( Costanza.packIDs[ t ] ) );
					lock.touchable		= lock.visible;		
					lock.scaleWhenDown 	= 1.0;
					lock.addEventListener(starling.events.Event.TRIGGERED, Pack_Selected);
					scroller.addChild( lock );
					packLocks.push( lock );
				}
			}
						
			appsSparkle = new PDParticleSystem(Root.assets.getXml("particle"), Root.assets.getTexture("texture"));
			
			this.addChild(appsSparkle);
			Starling.juggler.add(appsSparkle);
					
			//Setings.
			Settings_Box 				= new Settings( assets );
			Settings_Box.x 				= ((1366 / 2) - (Settings_Box.width / 2));
			Settings_Box.y 				= ((768 / 2) - (Settings_Box.height / 2));
			Settings_Box.visible 		= false;
			Settings_Box.touchable 		= false;
			addChild(Settings_Box);

			//Listeners.
			this.addEventListener(Root.DISPOSE,  disposeScene);	
			this.addEventListener(TouchEvent.TOUCH, UpdateMe);			
			this.addEventListener(Settings.HIDE_SETTINGS,  Update_Options);
			this.addEventListener(Settings.UPDATE_MUSIC,  Update_Music_Volume);
			
			Starling.current.nativeOverlay.addEventListener(flash.events.Event.ACTIVATE, FocusGained);	
			
			//Build Completed.
			dispatchEventWith(Root.SCENE_LOADED, true);
			
			App.backPressed.add( BackPressed );
			MoreApps.closing.add( PopupClosing );
			ForParentsScreen.closing.add( PopupClosing );
        }
		
		private function BackPressed():void
		{
			App.Log( "Handling back pressed in main menu" );
			if ( Settings_Box.visible ) { Update_Options(); }
			else if ( this.contains( HelpPage ) ) { CloseHelp(); }
			else if ( this.contains( CreditsPage ) ) { CloseCredits(); }
			else if ( DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP ) { App.ExitApp(); }
			else { App.CheckClose(); }
		}
		
		private function PopupClosing():void { App.backPressed.add( BackPressed ); }
		
		private function UpdateMe(e:TouchEvent):void{
						
			if(Settings_Box.visible){return;}
			switch(e.touches[0].phase){
				case "began":{	
					if(scrolling){scrolling = false;}
					firstX = e.touches[0].globalX;
				}break;
				case "moved":{	
					if ( Math.abs( e.touches[0].globalX - firstX ) > 5 )
					{
						scrolling = true;
						trace(e.touches[0].globalY + " Hmm");
					}
					
					if((e.touches[0].globalY > 248) && (e.touches[0].globalY < 662)){						
						var Force:int = (e.touches[0].globalX - lastX);
						scroller.x += Force;
						if(scroller.x < Costanza.MENU_LEFT ){scroller.x = Costanza.MENU_LEFT;}
						if(scroller.x > 0){scroller.x = 0;}
					}
					
				}break;
				case "ended":{	
					
				}break;
			}
			
			lastX = e.touches[0].globalX;
		}
		
		private function disposeScene():void
		{	
			//Test.
			trace("DISPOSED MENU");	
			
			IAPManager.actionCompleted.remove( RestoreComplete );
			IAPManager.manifestUpdated.remove( CheckPacks );
			MoreApps.loadedCallback = null;
			TweenMax.killDelayedCallsTo( loadSparklyAppButton );
			
			if ( blurFilter != null ) { blurFilter.dispose(); }
			
			//Listeners.
			this.removeEventListener(TouchEvent.TOUCH, UpdateMe);		
			this.removeEventListener(Root.DISPOSE, disposeScene);				
			this.removeEventListener(Settings.HIDE_SETTINGS, Update_Options);
			this.removeEventListener(Settings.UPDATE_MUSIC,  Update_Music_Volume);
			Starling.current.nativeOverlay.removeEventListener(flash.events.Event.ACTIVATE, FocusGained);		
			
			//this.removeChild(scroller);
			
			for each ( var btn:ButtonAlpha in packButtons )
			{
				btn.removeEventListener( starling.events.Event.TRIGGERED, Pack_Selected );
				btn.dispose();
			}
			
			CONFIG::FREE
			{
				for each ( var lock:ButtonAlpha in packLocks )
				{
					lock.removeEventListener( starling.events.Event.TRIGGERED, Pack_Selected );
					lock.dispose();
				}
				
				restoreBtn.removeEventListener(starling.events.Event.TRIGGERED,restoreTapped);
				restoreBtn.dispose();
			}
			
			
			MoreApps_BTN.removeEventListener(starling.events.Event.TRIGGERED, MoreApps_Selected);
			Parents_BTN.removeEventListener(starling.events.Event.TRIGGERED, ParentsHelp_Selected);
			Options_BTN.removeEventListener(starling.events.Event.TRIGGERED, Options_Selected);
			
			scroller.dispose();
			Parents_BTN.dispose();
			MoreApps_BTN.dispose();
			Options_BTN.dispose();
			
			//Delete Settings.
			this.removeChild(Settings_Box);
			Settings_Box.dispose();				
			
			//Delete More Apps Holder and ParentsHelp Holder.
			for(var X:int = 0; X < Starling.current.nativeOverlay.numChildren; X++){
				Starling.current.nativeOverlay.removeChildAt(X);
				X--;
			}
			
			MoreApps_Holder 	= null;
			ParentsHelp_Holder 	= null;		
			
			HelpPage.Dispose();
			CreditsPage.Dispose();
			
			//Delete Background.
			this.removeChild(Background);
			Background.dispose();	
			
			if ( loader != null )
			{
				loader.cancel();
				loader = null;
			}
			
			if ( appsSparkle != null )
			{
				Starling.juggler.remove(appsSparkle);
				//this.removeChild(appsSparkle,true);
				//appsSparkle.removeFromParent();
			}
			
			App.backPressed.remove( BackPressed );
			MoreApps.closing.remove( PopupClosing );
			ForParentsScreen.closing.remove( PopupClosing );
			
			//Destroy assets.
			assets.purge();
			assets.dispose();
			assets = null;
		}
		
		private function FocusGained(e:flash.events.Event):void{
			if(!Starling.current.stage3D.visible){
				Starling.current.stop();
			}
		}
		//--------------------------------------------------------------------------------
		
		private function Pack_Selected(e:starling.events.Event):void
		{
			if(scrolling || Settings_Box.visible || HelpPage.isActive() || CreditsPage.isActive() || restoring){return;}
			
			var packIndex:int = Costanza.packNames.indexOf( ( e.target as ButtonAlpha ).name )
			if ( packIndex > -1 )
			{
				soundManager.stopSound("Menu");
				
				if ( packIndex > 0 && CONFIG::FREE && packLocks[ packIndex ].visible )
				{
					soundManager.playSound("forwardButton", Costanza.soundVolume);		
					dispatchEventWith( Root.LOAD_SUBMENU, true, "");
				}
				else
				{
					var lbl:String = Costanza.packLabels[ packIndex ];
					soundManager.playSound( Costanza.packNames[ packIndex ] + "_Selection", Costanza.soundVolume);
					dispatchEventWith( Root.LOAD_PACK, true, lbl );	
				}
			}	
		}
		
		private function restoreTapped(e:starling.events.Event):void
		{
			if(Settings_Box.visible || restoring){return;}
			if ( blurFilter == null ) { blurFilter = new BlurFilter( 1, 1, .5 ); }
			this.filter = blurFilter;
			restoring = true;
			IAPManager.Restore();
		}
		
		private function RestoreComplete( success:Boolean ):void { this.filter = null; restoring = false; }
		
		private function CheckPacks():void
		{
			trace( "CheckPacks() called" );
			
			for ( var t:int = 1 ; t < Costanza.packNames.length ; t++ )
			{
				if ( packLocks[ t ].visible && ( IAPManager.IsPurchased( Costanza.packIDs[ t ] ) || IAPManager.IsPurchased( "bundle_pack" ) ) )
				{
					packLocks[ t ].visible		= false;
					packLocks[ t ].touchable	= false;
				}
			}
		}
		
		//--------------------------------------------------------------------------------
		//More Apps.
		//--------------------------------------------------------------------------------
		private function MoreApps_Selected(e:starling.events.Event):void{
			//Bounds.
			if(scrolling || Settings_Box.visible || HelpPage.isActive() || CreditsPage.isActive() || restoring){return;}
			App.backPressed.remove( BackPressed );
			soundManager.playSound("forwardButton", Costanza.soundVolume);
			//Clean native stage.
			if(Starling.current.nativeOverlay.numChildren > 0){
				trace(Starling.current.nativeOverlay.numChildren + " Children to many in native stage.");
				for(var X:int = 0; X < Starling.current.nativeOverlay.numChildren; X++){
					Starling.current.nativeOverlay.removeChildAt(X);
					X--;
				}
			}
			
			//Stop the 3d.
			//Starling.current.stage3D.visible = false;
			Starling.current.stop();			
			
			//Populate mroe Holder.
			Starling.current.nativeOverlay.addChild(MoreApps_Holder);
						
			//Show More Apps.
			MoreApps.Show(MoreApps_Holder)
		}
		
		private function MoreApps_Loaded():void{
			
			if ( MoreApps.moreAppsLoaded && MoreApps.apps.length > 0 && assets != null )
			{
				//Test.
				trace("We have more apps available.");
				
				//Show icaon we have access.
				MoreApps_BTN.visible	= true;
				MoreApps_BTN.alpha		= 0;
				MoreApps_BTN.enabled	= true;
				
				Parents_BTN.visible		= true;
				Parents_BTN.alpha		= 0;
				Parents_BTN.enabled		= true;
				
				TweenMax.allTo( [ MoreApps_BTN, Parents_BTN ], 1 , {alpha: 1} );
				TweenMax.delayedCall(3,loadSparklyAppButton);
			}
		}
		
		private function loadSparklyAppButton():void
		{
			loader = new LoaderMax( { onComplete:showSparklyAppButton } );
			
			ldr = new ImageLoader("app-storage:/" + MoreApps.apps[0].fileName );
			
			//App.Log( "Processing apps..." );
			trace("app-storage:/" + MoreApps.apps[0].fileName);
			loader.append( ldr ); 
			
			loader.load();
		}
		
		private function showSparklyAppButton(e:LoaderEvent):void
		{
			if ( assets != null )
			{
				var bmd:Bitmap = ldr.rawContent;
				
				appImageDisplay = new Button(Texture.fromBitmap(bmd));
				appImageDisplay.x = 200;
				appImageDisplay.y = 0 - appImageDisplay.height;
				appImageDisplay.addEventListener(starling.events.Event.TRIGGERED,MoreApps_Selected);
				this.addChild(appImageDisplay);
				TweenMax.to(appImageDisplay,.8,{y:20,delay:0.6,ease:Bounce.easeOut, onComplete:showSparkles});
				
				loader.dispose(true);
				loader = null;
				
				function showSparkles():void
				{
					appsSparkle.emitterX = appImageDisplay.x + appImageDisplay.width/2; 
					appsSparkle.emitterY = appImageDisplay.y + appImageDisplay.height; 
					appsSparkle.start();
					appsSparkle.parent.setChildIndex(appsSparkle,numChildren-1);
				}
			}
		}
		//--------------------------------------------------------------------------------
		
		//--------------------------------------------------------------------------------
		//Parents Help.
		//--------------------------------------------------------------------------------
		private function ParentsHelp_Selected(e:starling.events.Event):void{
			if(scrolling || Settings_Box.visible || HelpPage.isActive() || CreditsPage.isActive() || restoring){return;}
			App.backPressed.remove( BackPressed );
			soundManager.playSound("forwardButton", Costanza.soundVolume);
			
			//Clean native stage.
			if(Starling.current.nativeOverlay.numChildren > 0){
				trace(Starling.current.nativeOverlay.numChildren + " Children to many in native stage.");
				for(var X:int = 0; X < Starling.current.nativeOverlay.numChildren; X++){
					Starling.current.nativeOverlay.removeChildAt(X);
					X--;
				}
			}
			
			//Stop the 3d.
			Starling.current.stop();			
			
			//Populate Parents Help Holder.
			Starling.current.nativeOverlay.addChild(ParentsHelp_Holder);
			
			//Show Parents Help.
			
			ForParentsScreen.Show(ParentsHelp_Holder, this);
		}
		
		public function ParentsHelp_Completed():void{
			
			//Test.
			trace("Parents Help Completed.");	
			
			//Play_Button.touchable 	= true;					
			//this.parent.setChildIndex(this, (this.numChildren - 1));
			//this.touchable 			= true;
			
		}
		//--------------------------------------------------------------------------------
		
		//--------------------------------------------------------------------------------
		//Options.
		//--------------------------------------------------------------------------------
		private function Options_Selected(e:starling.events.Event):void{
			if(scrolling || Settings_Box.visible || HelpPage.isActive() || CreditsPage.isActive() || restoring){return;}
			soundManager.playSound("forwardButton", Costanza.soundVolume);
			//Activate_Show_Menu_Phase();
			//Pack_Management();
			//return;
			Update_Options();			
		}
		private function Update_Music_Volume(e:starling.events.Event):void{
			soundManager.setVolume("Menu", Costanza.musicVolume);
		}
		private function Update_Options():void{
			
			Settings_Box.visible 		= !Settings_Box.visible;
			Settings_Box.touchable 		= !Settings_Box.touchable;
			//Play_Button.touchable 		= More_Apps.touchable = Options_Button.touchable = Parents_Button.touchable = !Settings_Box.visible;
			
			this.setChildIndex(Settings_Box, (this.numChildren - 1));	
			
			soundManager.setVolume("Menu", Costanza.musicVolume);
		}		
		
		//--------------------------------------------------------------------------------
		//Maybe section.
		//--------------------------------------------------------------------------------
		private function TouchHelp(e:TouchEvent):void{
			
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN){
				
				trace(touch.globalX + " " + touch.globalY);
				
				var button_size:int			= 100;
				var top_offset:int			= int( button_size / 2 );
				var left_offset:int			= int( top_offset - Costanza.STAGE_OFFSET );
				
				var Rec_Home:Rectangle 		= new Rectangle(1052 - left_offset, 163 - top_offset , button_size, button_size);
				var Rec_Toggle:Rectangle 	= new Rectangle(938 - left_offset, 163 - top_offset, button_size, button_size);
								
				//Rec_Home.
				if ( Rec_Home.contains( touch.globalX, touch.globalY ) ) { CloseHelp(); CloseCredits(); }
				
				//Rec_Toggle.
				if ( Rec_Toggle.contains( touch.globalX, touch.globalY ) ) { TogglePage(); }	
			}				
		}
		
		public function TogglePage():void
		{
			if ( this.contains( HelpPage ) )
			{
				CloseHelp();
				CreditsPage.Activate();
				this.addChild(CreditsPage);
				CreditsPage.addEventListener(TouchEvent.TOUCH, TouchHelp);
			}
			else
			{
				CloseCredits();
				HelpPage.Activate();
				this.addChild(HelpPage);
				HelpPage.addEventListener(TouchEvent.TOUCH, TouchHelp);	
			}
		}
		
		public function CloseHelp():void
		{
			if ( this.contains( HelpPage ) )
			{
				HelpPage.removeEventListener(TouchEvent.TOUCH, TouchHelp);
				HelpPage.Deactivate();
				this.removeChild(HelpPage);	
			}
		}
		
		public function CloseCredits():void
		{
			if ( this.contains( CreditsPage ) )
			{
				CreditsPage.removeEventListener(TouchEvent.TOUCH, TouchHelp);
				CreditsPage.Deactivate();
				this.removeChild(CreditsPage);
			}
		}
		

    }
}