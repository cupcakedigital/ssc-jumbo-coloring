package{
	
    import com.cupcake.DeviceInfo;
    import com.cupcake.IAPManager;
    import com.cupcake.Utils;
    import com.distriqt.extension.message.Message;
	import com.greensock.easing.Linear;
    import com.greensock.TimelineMax;
    import com.greensock.TweenMax;
    
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.system.System;
    
    import scenes.Menu;
    import scenes.Pack_Selection;
    import scenes.Splash_Video;
    import scenes.ColoringBook.Coloring_Book_Scene;
    
    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.extensions.SoundManager;
    import starling.textures.Texture;
    import starling.utils.AssetManager;

    //Root of all scenes.
    public class Root extends Sprite
	{
		//global variables
        private static var sAssets:AssetManager;
		
        private var mSceneContainer:Sprite;
		private var mActiveScene:Sprite;
        private var soundManager:SoundManager;

		private var rotationSector:int				= 0;
		private var splashTimeline:TimelineMax;		
		private var Splash_Loader:Sprite;	
		private var Splash_Image:Image;
		private var splashCupcakeImage:Button;		
			
		public static const LOAD_COLORING:String 	= "loadColoring";
		public static const LOAD_MENU:String 		= "loadMenu";
		public static const LOAD_SUBMENU:String 	= "loadSubMenu";
		public static const LOAD_PACK:String		= "loadPack";
		public static const DISPOSE:String 			= "dispose";
		public static const SCENE_LOADED:String 	= "sceneLoaded";
		
		public static var Accessable_Content:Array;

		private var Current_Page_Pack:String;
		
        public function Root()
		{
			Current_Page_Pack = "";
						
			addEventListener(LOAD_MENU,  			onLoadMenu);
			addEventListener(LOAD_PACK,				onLoadPack);
			addEventListener(LOAD_SUBMENU, 			onLoadSubMenu);			
			addEventListener(LOAD_COLORING,			onLoadColoring);
			addEventListener(SCENE_LOADED,			onSceneLoaded);					

			Message.init(Costanza.DEV_KEY);

			DeviceInfo.Init();
           
			trace("IAP INIT");
			//IAP initialization
			
			CONFIG::DEBUG	{ IAPManager.devMode = true; }
			CONFIG::FULL	{ IAPManager.bypassMode = true; }
			
			IAPManager.catalogLoaded.add( IAPSetupCompleted );
			IAPManager.Init( CONFIG::MARKET );
			IAPManager.RevokeAll();
        }
		
		public function IAPSetupCompleted():void
		{
			if ( IAPManager.enabled ) 
			{ 
				if ( IAPManager.devMode ) { IAPManager.RevokeAll(); }
				trace( "IAP Setup successful" );
			}
			else { trace( "IAP Setup failed" ); }
		}
        
        public function start(background:Texture, assets:AssetManager):void{
			
			//Assets.
            sAssets 		= assets;
            
			//Sounds.
			soundManager 	= SoundManager.getInstance();

			//Load the Splash image. and populate it.
			Splash_Image 	= new Image(background);
			Splash_Loader	= new Sprite;
			Splash_Loader.addChild( Splash_Image );
			
			trace(Splash_Loader.x + "-------------------------------------------------------");
			addChild(Splash_Loader);   
			
			Splash_Image.x	= -( Splash_Loader.width / 2 );
			Splash_Image.y	= -( Splash_Loader.height / 2 );
			Splash_Loader.x = Costanza.STAGE_WIDTH / 2;
			Splash_Loader.y = Costanza.STAGE_HEIGHT / 2;
			TweenMax.to( Splash_Loader, 1, { shortRotation:{ rotation:Utils.toRadians(270), useRadians:true }, ease:Linear.easeNone, onComplete: SpinLoader } );

			//Process are loaded data.            
            assets.loadQueue(function onProgress(ratio:Number):void{
                
				//If Loaded.
                if(ratio == 1){
				
					//Grab Sounds.
					soundManager.addSound("Menu", 			Root.assets.getSound("Menu"));
					soundManager.addSound("backwardButton",	Root.assets.getSound("backward button"));
					soundManager.addSound("forwardButton",	Root.assets.getSound("forward button"));

					Load_Saved_Content();
				};
            });
        }			
		
		private function First_Load():void{			
			
			trace("First Load Active.");			
			//Create the Scene.
			mSceneContainer = new Sprite();
			mSceneContainer.x = Costanza.STAGE_OFFSET;
			trace(mSceneContainer.x);
			addChild(mSceneContainer);
			
			//Populate the Scene.
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP ) { showScene(Menu); }
			else
				{ showScene(Splash_Video); }
			
			// now would be a good time for a clean-up 
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
		}
		
		private function SpinLoader():void
		{
			var newRot:Number;
			rotationSector++;
			
			switch ( rotationSector )
			{
				case 1:		newRot = 180;	break;
				case 2:		newRot = 90;	break;
				case 3:		newRot = 0;		break;
				case 4:		newRot = 270;	rotationSector = 0;	break;
				default:	trace( "Rotation not matching, good job... rotation: " + Splash_Loader.rotation + " and -90 in radians: " + Utils.toRadians(-90) );
			}
			TweenMax.to( Splash_Loader, 1, { shortRotation:{rotation: Utils.toRadians(newRot), useRadians:true}, ease:Linear.easeNone, onComplete: SpinLoader } );
		}
		
		
		private function Load_Saved_Content():void{
						
			//Extension.
			var FilePath:String = ("Store_Content_Accessabillity_Data.xml");			
			//Create the file.
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );
			
			var Sticker_XML:XML;
			var stream:FileStream = new FileStream();
			
			if(myFile.exists){					
				
				stream.open(myFile, FileMode.READ);	
				Sticker_XML = XML(stream.readUTFBytes(stream.bytesAvailable));
				stream.close();	
				There_Is_Saved_Content(Sticker_XML);
				
			}else{				
				Create_Saved_Content();
			}	
			
			//clear for Leaks.
			stream = null;
			Sticker_XML = null;			
			myFile = null;
			FilePath = null;			
			
		}
		
		private function There_Is_Saved_Content(TheXML:XML):void
		{
			Accessable_Content = new Array();
			Accessable_Content = [0, 0, 0, 0, 0, 0];
			
			for(var X:int = 0; X < TheXML.ThisPack.length(); X++){					
				Accessable_Content[X] = int(TheXML.ThisPack[X].unlocked.text());				
			}	
			
			First_Load();
		}
		
		private function Create_Saved_Content():void
		{
			Accessable_Content = new Array();			
			
			//Packs unlocked.
			Accessable_Content = [0, 1, 0, 0, 0, 0];	
			
			//Save it.
			Save_Saved_Content();
			
			First_Load();
		}
		
		public static function Save_Saved_Content():void
		{			
			
			var Accessable_Content_XML:String	= "<AC>\n";
			
			for(var X:int = 0; X < Accessable_Content.length; X++)
			{
				Accessable_Content_XML = Accessable_Content_XML + "<ThisPack>\n";
				Accessable_Content_XML = Accessable_Content_XML + "<unlocked>" + Accessable_Content[X] + "</unlocked>" + "\n";
				Accessable_Content_XML = Accessable_Content_XML + "</ThisPack>\n";
			}
			
			Accessable_Content_XML = Accessable_Content_XML + "</AC>";			
			
			//Extension.
			var FilePath:String = ("Store_Content_Accessabillity_Data.xml");		
			
			//Create the file.
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );				
			
			// create filestream
			var stream:FileStream = new FileStream();
			
			//  open/create the file, set the filemode to write in order to save.
			stream.open( myFile , FileMode.WRITE);
			
			// write your byteArray into the file.
			stream.writeUTFBytes(Accessable_Content_XML.toString());
			
			// close the file.
			stream.close();			
			
			//Clear for Leaks.
			stream 		= null;
			myFile 		= null;
			FilePath 	= null;
			Accessable_Content_XML = null;
		}
		
		private function onSceneLoaded(event:Event):void
		{
			trace("SCENE LOADED");
			
			//TweenMax.to(transitionImage,0.2,{alpha:0, delay:0.4, onComplete:hideTransition});
			function hideTransition():void
			{
				//transitionImage.touchable = false;
			}
		}

		private function onEnterSubMenu(event:Event, gameMode:String):void
		{
			trace("Entering SubMenu! Mode: " + gameMode);
			showScene(Pack_Selection);
		}

		private function onLoadMenu(event:Event):void
		{
			trace("Loading Main Lobby");
			showScene(Menu);
		}

		private function onLoadSubMenu(event:Event):void
		{
			trace("Loading Sub Menu");
			showScene(Pack_Selection);
		}
		
		private function onLoadPack( event:Event ):void
		{
			var pack:String = event.data as String;
			if ( Costanza.packLabels.indexOf( pack ) > -1 )
			{
				trace( "Now loading pack: " + event.data );
				Current_Page_Pack = event.data as String;
				showScene(Coloring_Book_Scene);
			}
		}

		private function onLoadColoring(event:Event):void{ showScene(Coloring_Book_Scene); }

        public function showScene(screen:Class):void { addPage(screen); }

		private function addPage(screen:Class, num:int=-1):void
		{
			Starling.juggler.delayCall(function():void
			{
				if (mActiveScene){
					mActiveScene.broadcastEventWith(DISPOSE);
					mActiveScene.removeFromParent(true)
				};
				if(num > -1){mActiveScene = new screen(num);}
				else{mActiveScene = new screen(Current_Page_Pack);}
				mSceneContainer.addChild(mActiveScene);
			},0.2);
		}

        public static function get assets():AssetManager { return sAssets; }
    }
}