package utils.ParticleSystem
{
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.deg2rad;
	
	import utils.MathHelpers;

	public class ParticleEmitter extends Sprite
	{
		//private variables
		private var particleImage:Image; //this is the image were going to be displaying, we only need 1 copy
		private var particleCount:int; //the amount of particles were going to throw
		private var particleArray:Array = new Array(); //array to hold all the particles
		
		private var useGravity:Boolean;
		private var lastTime:Number;
		private var currentTime:Number;
		
		public var GRAVITY:Number = 500;
		public var ROTATE:Number = 5;

		public function ParticleEmitter(image:Image, gravity:Boolean = true)
		{
			//store our image, we can change this to a vector of images and
			//randomly pick among the images to use as part of the vectors, but
			//lets keep it simple for now
			particleImage = image;
			useGravity = gravity;
			lastTime = getTimer();
			addEventListener(Event.ENTER_FRAME, Update);
		}

		public function Update(e:Event):void
		{
			//update the Timer
			currentTime = getTimer();
			var timePassed:Number = (currentTime - lastTime) * 0.001;
			//Update all the particles if any are alive
			if(particleArray.length > 0)
			{
				//go through the list backwards, to prevent fallback on removals
				for(var i:int = particleArray.length -1 ; i >= 0; i--)
				{
					//update the particles position based on trajectory and gravity(if used).
					//also reduce the lifespan
					//this is acceleration, unimplemented at the moment
					//particleArray[i].velocity.x += particleArray[i].velocity.x * timePassed;
					//particleArray[i].velocity.y += particleArray[i].velocity.y * timePassed;

					if(useGravity)
					{
						//if were using gravity, pull the object more downwards
						particleArray[i].velocity.y += (GRAVITY * timePassed);
					}

					particleArray[i].x += particleArray[i].velocity.x * timePassed;
					particleArray[i].y += particleArray[i].velocity.y * timePassed;

					//if this is a rotating particle, rotate it
					if(particleArray[i].rotate)
					{
						if(particleArray[i].velocity.x >= 0)
							particleArray[i].rotation += deg2rad(ROTATE);
						else
							particleArray[i].rotation += deg2rad(-ROTATE);
					}
					particleArray[i].lifeSpan -= timePassed;
					if(particleArray[i].lifeSpan < 0.5)
					{
						if(particleArray[i].fadeOut)//start a fade out near the end of it's life
						{
							particleArray[i].alpha -= 2 * timePassed;
						}
						if(particleArray[i].suddenSlow)//start the sudden slow near the end of it's life
						{
							particleArray[i].velocity.x -= particleArray[i].velocity.x * timePassed;
							particleArray[i].velocity.y -= particleArray[i].velocity.y * timePassed;
						}
					}
					if(particleArray[i].lifeSpan <= 0.0)
					{
						KillMe(i);
					}
				}
			}

			//set our current time to our last time for next frame
			lastTime = currentTime;
		}

		private function randomRange(minNum:Number, maxNum:Number):Number
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}

		private function KillMe(index:int):void
		{
			trace("removing particle: " + index);
			removeChild(particleArray[index], true);
			particleArray.splice(index,1);
		}

		//PUBLIC FUNCTIONS
		public function CreateBurst(count:int, forceVarianceX:Number = 100, forceVarianceY:Number = 100, lifeSpan:Number = 1.25, fadeOut:Boolean = false, rotate:Boolean = false):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(randomRange(-forceVarianceX, forceVarianceX), randomRange(-forceVarianceY, forceVarianceY));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImage.texture), velocity, lifeSpan);
				if(fadeOut)
				{
					particle.fadeOut = fadeOut;
				}
				particle.rotate = rotate;
				particle.touchable = false;
				addChild(particle);
				particleArray.push(particle);
			}
		}

		public function CreateVerticalBurst(count:int, forceVarianceX:Number = 100, forceVarianceY:Number = 100, lifeSpan:Number = 1.25, fadeOut:Boolean = false, rotate:Boolean = false):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(randomRange(-forceVarianceX, forceVarianceX), randomRange(forceVarianceY - 100, forceVarianceY + 100));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImage.texture), velocity, lifeSpan);
				if(fadeOut)
				{
					particle.fadeOut = fadeOut;
				}
				particle.rotate = rotate;
				particle.touchable = false;
				addChild(particle);
				particleArray.push(particle);
			}
		}

		public function CreateHorizontalBurst(count:int, forceVarianceX:Number = 100, forceVarianceY:Number = 100, lifeSpan:Number = 1.25, fadeOut:Boolean = false, rotate:Boolean = false, randSize:Boolean = false, slow:Boolean = false):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(randomRange(forceVarianceX - 100, forceVarianceX + 100) , randomRange(-forceVarianceY, forceVarianceY));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImage.texture), velocity, lifeSpan);
				if(fadeOut)
				{
					particle.fadeOut = fadeOut;
				}
				if(randSize)
				{
					var range:Number = MathHelpers.randomNumberRange(0.4, 0.8);
					particle.scaleX = range;
					particle.scaleY = range;
				}
				if(slow)
				{
					particle.suddenSlow = true;
				}
				particle.rotate = rotate;
				particle.touchable = false;
				addChild(particle);
				particleArray.push(particle);
			}
		}
	}
}