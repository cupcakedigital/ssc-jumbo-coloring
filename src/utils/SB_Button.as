package utils{		
	
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	public class SB_Button extends Sprite{		
		
		//Image Background.
		private var Plate_Image:ButtonAlpha;
		
		//Button Text.
		private var Plate_Text:Starling_TextBox;
		
		//Trigger function.
		private var Trigger:Function;
		
		//Bounds.
		private var Size_Rec:Rectangle;
		
		public function SB_Button(pAssets:AssetManager, ImageName:String, DisplayText:String, TextGlow:uint, TextColor:uint, FontName:String, FontSize:int, newTrigger:Function = null, Xoff:int = 0, Yoff:int = 0){
			
			super();			
			
			//Create the image.
			Plate_Image				= new ButtonAlpha(pAssets.getTexture(ImageName));
			Plate_Image.x 			= -(Plate_Image.width  / 2);
			Plate_Image.y 			= -(Plate_Image.height / 2);
			Plate_Image.touchable 	= true;		
			Plate_Image.scaleWhenDown = 1.0;
			
			//Bounds.
			Size_Rec = Plate_Image.bounds;
			
			//Create the text.
			Plate_Text 				= new Starling_TextBox(DisplayText, FontSize, FontName, TextGlow, TextColor, 20, Plate_Image.width, Plate_Image.height);
			Plate_Text.x 			= Xoff;
			Plate_Text.y 			= Yoff;
			Plate_Text.touchable 	= false;	
			
			//Reset trigger.
			Trigger = newTrigger;
			if(Trigger != null){Plate_Image.addEventListener(starling.events.Event.TRIGGERED, Trigger);}
			
			//Populate.
			this.addChild(Plate_Image);
			this.addChild(Plate_Text);
			
		}
		
		//Get Bounds.
		public function myBounds():Rectangle{return Size_Rec;}
		
		//Clean.
		public function Dispose():void{
			
			//Reset Trigger if nedded.
			if(Trigger != null){
				Plate_Image.removeEventListener(starling.events.Event.TRIGGERED, Trigger);
				Trigger = null;
			}
			
			//Depopulate.
			this.removeChild(Plate_Text);
			this.removeChild(Plate_Image);
			
			//Burger time.
			Size_Rec = null;
			
			//Clean.
			Plate_Text.Dispose();
			Plate_Image.dispose();
		}
		
		public function New_Trigger(newTrigger:Function):void{
			
			//Reset Trigger if nedded.
			if(Trigger != null){
				Plate_Image.removeEventListener(starling.events.Event.TRIGGERED, Trigger);
				Trigger = null;
			}
			
			//New Trigger.
			Trigger = newTrigger;
			Plate_Image.addEventListener(starling.events.Event.TRIGGERED, Trigger);
			
		}
		
	}
}