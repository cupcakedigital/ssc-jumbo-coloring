package utils{
	
	import flash.filters.GlowFilter;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class Starling_TextBox extends Sprite{	
		
		private var TextBox:TextField;
		private var Glow:GlowFilter;
		
		public function Starling_TextBox(Words:String = "", Size:int = 30, Family:String = "Latino_Rumba", BC:uint = 0x000000, FC:uint = 0xffffff, StrokeThinkness:int = 1, Width:int = 200, Height:int = 200){
			
			super();			
			
			var Final_Word:String = "\n" + Words + "\n";
			
			Glow 					= new GlowFilter(BC, 0.9, 2, 2, 10, 3, false, false);
			TextBox 				= new TextField(Width, Height, Words, Family, Size, FC);	
			TextBox.border 			= false;
			TextBox.hAlign 			= HAlign.CENTER;
			TextBox.vAlign 			= VAlign.CENTER;
			TextBox.nativeFilters 	= [Glow];			
			TextBox.x 				= int(-(TextBox.width  / 2) - 0);
			TextBox.y 				= int(-(TextBox.height / 2) - 0);
			TextBox.visible 		= true;
			
			this.addChild(TextBox);
			this.touchable = false;
						
		}
		
		public function Dispose():void{
			
			//Clean Box.
			this.removeChild(TextBox);
			TextBox.nativeFilters = [];
			TextBox.dispose();
			Glow = null;
			
		}
		
	}
}