package utils  
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Elastic;
	import com.reyco1.physinjector.manager.Utils;
	
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;

	public class Wobbler extends Sprite
	{
		private var img:Image;
		
		private var isDragging:Boolean = false;
		private var isTweening:Boolean = false;
		private var offsetx:Number;
		private var offsety:Number;
		private var tween:TweenMax;
		
		private var sm:SoundManager;
		
		private var tugDistance:int = 75;
		
		public function Wobbler(texture:Texture,_X:Number, _Y:Number)
		{
			sm = SoundManager.getInstance();
			
			this.x = offsetx = _X;
			this.y = offsety = _Y;
			img = new Image(texture);
			img.pivotX = img.width/2;
			img.pivotY = img.height/2;
			addChild(img);
			
			img.addEventListener(TouchEvent.TOUCH,dragImg);
		}
		
		private function dragImg(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch != null && !isTweening)
			{
				var mc:MovieClip = e.currentTarget as MovieClip;
				var mouse:Point = new Point(touch.globalX,touch.globalY);
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						isDragging = true;
						break;
					}
					case TouchPhase.MOVED:
					{
						if(isDragging)
						{
							this.x = mouse.x;
							this.y = mouse.y;
							
							//drag bounds
							if(this.x > offsetx+75){this.x = offsetx+75;}
							if(this.y > offsety+75){this.y = offsety+75;}
							if(this.x < offsetx-75){this.x = offsetx-75;}
							if(this.y < offsety-75){this.y = offsety-75;}
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(isDragging)
						{
							isTweening = true;
							if(this.x == offsetx && this.y == offsety){randomWobble();}
							else
							{
								sm.playSound("Wobble",Costanza.soundVolume);
								TweenMax.to(this,.5,{x:offsetx,y:offsety,ease:Elastic.easeOut, onComplete:setTweening});
							}
						}
						isDragging = false;
						break;
					}
				}
			}
		}//end dragImg

		private function randomWobble():void
		{
			var targetAngle:Number =  Math.floor(Math.random()*360);
			targetAngle = targetAngle*(Math.PI/180);
			
			TweenMax.to(this,0.2,{x:offsetx + Math.cos(targetAngle)*tugDistance, y:offsety + Math.sin(targetAngle)*tugDistance, onComplete:wobble});
		}
		
		private function wobble():void
		{
			sm.playSound("Wobble",Costanza.soundVolume);
			TweenMax.to(this,.5,{x:offsetx,y:offsety,ease:Elastic.easeOut, onComplete:setTweening});
		}
		
		private function setTweening():void
		{
			isTweening = false;
		}
		
		public function destroy():void
		{
			TweenMax.killTweensOf(this);
			img.removeEventListener(TouchEvent.TOUCH,dragImg);
			this.parent.removeChild(this,true);
		}
	
	}//end class
}//end package